/**
 * 
 */
package com.sdsl.opsbi.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.sdsl.opsbi.exceptions.DBException;
import com.sdsl.opsbi.metric.IMetric;
import com.sdsl.opsbi.utils.Configurator;

/**
 * @author Sudip Das
 *
 */
public class DBController {
	/**
	 * Default logger
	 */
	final static Logger logger = Logger.getLogger(DBController.class.getName());

	/**
	 * Connection
	 */
	private Connection conn = null;

	/**
	 * Prepared Statements
	 */

	private PreparedStatement getTopTenLobsPS;
	private PreparedStatement fetchTopTenLobsDataPS;
	private PreparedStatement insertTopTenLobsDataPS;
	private PreparedStatement deleteTopTenLobsPS;

	private PreparedStatement getVolumeDataNoGrpByPS;
	private PreparedStatement getVolumeDataGrpByPS;
	private PreparedStatement getTotalExtractsByLobOnlyPs;
	private PreparedStatement getTotalExtractsByLobByQsPs;

	private PreparedStatement fetchCarrierEnrichmentDataForCarrierDictPS;
	private PreparedStatement insertCarrierDictDataPS;
	private PreparedStatement deleteCarrierDictRecsPs;

	public DBController() throws DBException {
		getConnection();
	}

	/**
	 * Method to create a connection to DB
	 * 
	 * @return Connection to DB
	 * @throws ComparatorException Comparator Exception
	 * @throws DBException         DB Exception
	 */
	private Connection getConnection() throws DBException {
		try {
			logger.info("Connecting to Cloud DB instance.");

			PGresDb postGresDB = new PGresDb();
			conn = postGresDB.getConnection(Configurator.dbProps);

			controller_initialize();
			return conn;
		} catch (ClassNotFoundException e) {
			logger.fatal("ERROR: ClassNotFoundException in establising Cloud DB connection");
			throw new DBException("In getConnection()", e);
		} catch (SQLException e) {
			logger.fatal("ERROR: SQLException in establising Cloud DB connection.");
			throw new DBException("In getConnection()", e);
		}
	}

	/**
	 * @throws DBException
	 * 
	 */
	private void controller_initialize() throws DBException {

		try {

			String sqlQuery = "SELECT file_lob from toplobs";
			getTopTenLobsPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "select file_lob, sum(total_files) AS file_volume from opsbi_volume_coverage \n"
					+ "where file_lob is not null and qs_key is not null group by file_lob order by file_volume desc limit 10";
			fetchTopTenLobsDataPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "INSERT into toplobs VALUES(?)";
			insertTopTenLobsDataPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "DELETE FROM toplobs WHERE EXISTS (select *  from toplobs)";
			deleteTopTenLobsPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "select * from opsbi_volume_coverage where extracted_on between ?::date and ? :: date";
			getVolumeDataNoGrpByPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "select * from opsbi_volume_coverage where extracted_on between ?::date and ? :: date \n"
					+ "and file_lob is not null and qs_key is null";
			getVolumeDataGrpByPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "select file_lob, sum(total_extracts) as sum_extracts from opsbi_volume_coverage \n"
					+ "where file_lob is not null and qs_key is null group by file_lob";
			getTotalExtractsByLobOnlyPs = conn.prepareStatement(sqlQuery);

			sqlQuery = "select file_lob, qs_key, sum(total_extracts) as sum_qs_extracts from opsbi_volume_coverage \n"
					+ "where file_lob is not null and qs_key is NOT null group by file_lob, qs_key";
			getTotalExtractsByLobByQsPs = conn.prepareStatement(sqlQuery);

			sqlQuery = "SELECT DISTINCT epod_flattened.pack_id, epod_flattened.qs_value AS carriervalue, epod_flattened.qs_value_std AS carriervaluestd \n "
					+ "FROM epod_flattened WHERE epod_flattened.job_status_ts BETWEEN ? :: TIMESTAMP  AND ? :: TIMESTAMP \n "
					+ "AND epod_flattened.qs_key ='UC1'";
			fetchCarrierEnrichmentDataForCarrierDictPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "INSERT into opsbicarrierdict VALUES(?,?,?)";
			insertCarrierDictDataPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "DELETE FROM opsbicarrierdict WHERE EXISTS (select *  from opsbicarrierdict)";
			deleteCarrierDictRecsPs = conn.prepareStatement(sqlQuery);

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in controller initilization.");
			throw new DBException("In controller_initialize()", e);
		}
	}

	/**
	 * Releasing DB related resources
	 * 
	 * @throws DBException
	 */
	public void controller_finalize() throws DBException {
		try {
			if (fetchTopTenLobsDataPS != null) {
				fetchTopTenLobsDataPS.close();
				fetchTopTenLobsDataPS = null;
			}
			if (insertTopTenLobsDataPS != null) {
				insertTopTenLobsDataPS.close();
				insertTopTenLobsDataPS = null;
			}
			if (getVolumeDataNoGrpByPS != null) {
				getVolumeDataNoGrpByPS.close();
				getVolumeDataNoGrpByPS = null;
			}
			if (getVolumeDataGrpByPS != null) {
				getVolumeDataGrpByPS.close();
				getVolumeDataGrpByPS = null;
			}
			if (getTotalExtractsByLobOnlyPs != null) {
				getTotalExtractsByLobOnlyPs.close();
				getTotalExtractsByLobOnlyPs = null;
			}
			if (getTotalExtractsByLobByQsPs != null) {
				getTotalExtractsByLobByQsPs.close();
				getTotalExtractsByLobByQsPs = null;
			}
			if (fetchCarrierEnrichmentDataForCarrierDictPS != null) {
				fetchCarrierEnrichmentDataForCarrierDictPS.close();
				fetchCarrierEnrichmentDataForCarrierDictPS = null;
			}
			if (insertCarrierDictDataPS != null) {
				insertCarrierDictDataPS.close();
				insertCarrierDictDataPS = null;
			}
			if (deleteCarrierDictRecsPs != null) {
				deleteCarrierDictRecsPs.close();
				deleteCarrierDictRecsPs = null;
			}

			if (conn != null && !conn.isClosed()) {
				if (!conn.getAutoCommit()) {
					conn.commit();
					conn.setAutoCommit(true);
				}
				conn.close();
				conn = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in controller finalize.");
			throw new DBException("In controller_finalize()", e);
		}
	}

	/**
	 * Update Top Ten LOB data in DB
	 * 
	 * @throws DBException
	 */
	public void updateTopTenLobs() throws DBException {
		ArrayList<String> topTenLobs = new ArrayList<String>();
		try {
			ResultSet resultSet = fetchTopTenLobsDataPS.executeQuery();
			while (resultSet.next()) {
				topTenLobs.add(resultSet.getString("file_lob"));
			}

			deleteTopTenLobsPS.executeUpdate();
			insertTopTenLobs(topTenLobs);

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in controller finalize.");
			throw new DBException("In updateTopTenLobs()", e);
		}
	}

	/**
	 * Insert top ten scored LOBs
	 * 
	 * @param lobList
	 * @throws DBException
	 */
	public void insertTopTenLobs(ArrayList<String> lobList) throws DBException {
		logger.info("Inserting records in toplobs");
		final int batchSize = 10;
		int count = 0;
		try {
			conn.setAutoCommit(false);
			for (String eachLob : lobList) {
				insertTopTenLobsDataPS.setString(1, eachLob);
				insertTopTenLobsDataPS.addBatch();
				if (++count % batchSize == 0) {
					insertTopTenLobsDataPS.executeBatch();
					conn.commit();
				}
			}
			insertTopTenLobsDataPS.executeBatch();// Remaining records
			conn.commit();
			logger.info("Completed updating Top Ten Lobs Cube Data");
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in inserting Top Ten Lobs in toplobs Table");
			throw new DBException("In insertTopTenLobs()", e);
		}

	}

	/**
	 * Get Metric Volume Records for Non Group By (Files, Packs, Extracts, Pages)
	 * 
	 * @param startTs
	 * @param endTs
	 * @param metricId
	 * @return
	 * @throws DBException
	 */

	public Map<String, Integer> getMetricVolumeRecsNoGrpBy(Timestamp startTs, Timestamp endTs, int metricId)
			throws DBException {
		Map<String, Integer> sortedReturnMap = new TreeMap<String, Integer>();

		java.sql.Date startDate = new java.sql.Date(startTs.getTime());
		java.sql.Date endDate = new java.sql.Date(endTs.getTime());

		PreparedStatement volumePs = getVolumeDataNoGrpByPS;
		ResultSet resultSet = null;
		String volMetricColName = null;

		switch (metricId) {
		case IMetric.FILES:
			volMetricColName = "total_files";
			break;
		case IMetric.PACKS:
			volMetricColName = "total_packs";
			break;
		case IMetric.EXTRACTS:
			volMetricColName = "total_extracts";
			break;
//			//To be modified
//		case IMetric.PAGES:
//			volMetricColName = "totalpages";
//			break;
		}
		try {
			volumePs.setDate(1, startDate);
			volumePs.setDate(2, endDate);
			resultSet = volumePs.executeQuery();
			while (resultSet.next()) {
				sortedReturnMap.put(resultSet.getDate("extracted_on").toString(), resultSet.getInt(volMetricColName));
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException while fetching Volume Data No Group By from opsbi_volume_coverage Table");
			throw new DBException("In getMetricVolumeRecsNoGrpBy()", e);
		}
//		System.err.println(sortedReturnMap);
		return sortedReturnMap;

	}

	/**
	 * Get Metric Volume Records for Group By Lob (Files, Packs, Extracts, Pages)
	 * 
	 * @param startTs
	 * @param endTs
	 * @param metricId
	 * @param topLobs
	 * @return
	 * @throws DBException
	 */

	public Map<String, Integer> getMetricVolumeRecsGrpByLob(Timestamp startTs, Timestamp endTs, int metricId,
			ArrayList<String> topLobs) throws DBException {
		Map<String, Integer> sortedReturnMap = new TreeMap<String, Integer>();

		java.sql.Date startDate = new java.sql.Date(startTs.getTime());
		java.sql.Date endDate = new java.sql.Date(endTs.getTime());

		Multimap<String, Integer> lobWithFileCountListMMap = ArrayListMultimap.create();

		PreparedStatement volumePs = getVolumeDataGrpByPS;
		ResultSet resultSet = null;
		String volMetricColName = null;

		switch (metricId) {
		case IMetric.FILES:
			volMetricColName = "total_files";
			break;
		case IMetric.PACKS:
			volMetricColName = "total_packs";
			break;
		case IMetric.EXTRACTS:
			volMetricColName = "total_extracts";
			break;
//			//To be modified
//		case IMetric.PAGES:
//			volMetricColName = "totalpages";
//			break;
		}

		try {

			volumePs.setDate(1, startDate);
			volumePs.setDate(2, endDate);
			resultSet = volumePs.executeQuery();
			// To keep track of count for lobs that will be marked as others
			int othersTotalCount = 0;
			while (resultSet.next()) {
				String lob = resultSet.getString("file_lob").toUpperCase();

				if (topLobs.stream().anyMatch(lob::equalsIgnoreCase)) {
					int volCount = resultSet.getInt(volMetricColName);
					sortedReturnMap.put(lob, volCount);
					lobWithFileCountListMMap.put(lob, volCount);
				} else {
					othersTotalCount = othersTotalCount + resultSet.getInt(volMetricColName);
				}
			}
			lobWithFileCountListMMap.put("OTHERS", othersTotalCount);
		} catch (SQLException e) {
			logger.error(
					"ERROR: SQLException while fetching Volume Data Group By LOB from opsbi_volume_coverage Table");
			throw new DBException("In getMetricVolumeRecsGrpByLob()", e);
		}

		for (String eachLob : topLobs) {
			if (lobWithFileCountListMMap.get(eachLob) != null) {
				List<Integer> filecountGroup = (List<Integer>) lobWithFileCountListMMap.get(eachLob);
				int sum = filecountGroup.stream().mapToInt(Integer::intValue).sum();
				sortedReturnMap.put(eachLob, sum);
			} else {
				sortedReturnMap.put(eachLob, 0);
			}
		}

//		System.out.println("lobWithFileCountListMMap:"+lobWithFileCountListMMap);
//		System.err.println("final sortedReturnMap:"+sortedReturnMap);
		return sortedReturnMap;
	}

	/**
	 * Get top ten scored LOBs
	 * 
	 * @return List of LOBs
	 * @throws DBException
	 */
	public ArrayList<String> getTopTenLobs() throws DBException {
		ArrayList<String> topTenLobs = new ArrayList<String>();
		try {
			ResultSet resultSet = getTopTenLobsPS.executeQuery();
			while (resultSet.next()) {
				topTenLobs.add(resultSet.getString("file_lob"));
			}

			if (resultSet != null) {
				resultSet.close();
				resultSet = null;
			}

		} catch (SQLException e) {
			logger.error("ERROR: SQLException while fetching Top LOBs from toplobs Table");
			throw new DBException("In getTopTenLobs()", e);
		}
		return topTenLobs;
	}

	/**
	 * Method to get coverage related records. Calculates total extracts in each LOB
	 * and then total extracts per LOB per question. Provides a ratio of total
	 * extracts per LOB per question to total extracts per LOB. The data set is
	 * based on the corpus of one year record in flat table. It does not calculate
	 * against any other date.
	 * 
	 * @return Returns a MultiMap with question as Key and a map of name = LOB and
	 *         value as %percentage of total extracts per question per LOB to total
	 *         extracts per LOB
	 * @throws DBException
	 */
	public Multimap<String, LinkedHashMap<String, Object>> getMetricCoverageRecs() throws DBException {
		Multimap<String, LinkedHashMap<String, Object>> coverageDatarecords = LinkedHashMultimap.create();

		LinkedHashMap<String, Integer> totalExtractsPerLob = new LinkedHashMap<String, Integer>();

		try {
			ResultSet resultSet = getTotalExtractsByLobOnlyPs.executeQuery();
			while (resultSet.next()) {
				totalExtractsPerLob.put(resultSet.getString("file_lob").toString(), resultSet.getInt("sum_extracts"));
			}

			if (resultSet != null) {
				resultSet.close();
				resultSet = null;
			}

//			System.err.println("totalExtractsPerLob:" + totalExtractsPerLob);

			resultSet = getTotalExtractsByLobByQsPs.executeQuery();
			while (resultSet.next()) {
				String lob = resultSet.getString("file_lob");
				LinkedHashMap<String, Object> currentMap = new LinkedHashMap<String, Object>();
				String qs_key = resultSet.getString("qs_key");
				currentMap.put("name", lob);
				int sumExtractsPerLob = totalExtractsPerLob.get(lob);
				int sumExtractsPerLobPerQs = resultSet.getInt("sum_qs_extracts");
				double percentage = 100.0 * (sumExtractsPerLobPerQs * 1.0 / sumExtractsPerLob);
				percentage = Math.round(percentage * 100.0) / 100.0;
				currentMap.put("value", percentage);
				coverageDatarecords.put(qs_key, currentMap);
			}

			if (resultSet != null) {
				resultSet.close();
				resultSet = null;
			}

//			System.err.println("coverageDatarecords:" + coverageDatarecords);

		} catch (SQLException e) {
			logger.error("ERROR: SQLException while fetching Coverage Data for each question in each LOB");
			throw new DBException("In getMetricCoverageRecs()", e);
		}
		return coverageDatarecords;
	}

	/**
	 * Update Carrier Dictionary data in hypercube
	 * 
	 * @param minTs
	 * @param maxTs
	 * @throws DBException
	 */
	public void updateCarrierDictData(Timestamp minTs, Timestamp maxTs) throws DBException {
		logger.info("Fetching Carrier Dictionary Cube Data");
		ArrayList<LinkedHashMap<String, String>> carrierDictRecords = new ArrayList<LinkedHashMap<String, String>>();
		try {
			fetchCarrierEnrichmentDataForCarrierDictPS.setString(1, minTs.toString());
			fetchCarrierEnrichmentDataForCarrierDictPS.setString(2, maxTs.toString());
			ResultSet resultSet = fetchCarrierEnrichmentDataForCarrierDictPS.executeQuery();
			while (resultSet.next()) {
				LinkedHashMap<String, String> carrierDictDataMap = new LinkedHashMap<String, String>();
				String pack_id = resultSet.getString("pack_id");
				pack_id = pack_id == null ? "NO RECORDS" : pack_id;
				String carriervalue = resultSet.getString("carriervalue");
				carriervalue = carriervalue == null ? "NO RECORDS" : carriervalue;
				String carriervaluestd = resultSet.getString("carriervaluestd");
				carriervaluestd = carriervaluestd == null ? "NO RECORDS" : carriervaluestd;

				carrierDictDataMap.put("pack_id", pack_id);
				carrierDictDataMap.put("carriervalue", carriervalue);
				carrierDictDataMap.put("carriervaluestd", carriervaluestd);
				carrierDictRecords.add(carrierDictDataMap);
			}

			deleteCarrierDictRecsPs.executeUpdate();
			insertBatchCarrierDictData(carrierDictRecords);

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in controller finalize.");
			throw new DBException("In updateClientRegionData()", e);
		}
	}

	/**
	 * Insert Carrier Dictionary data in hypercube
	 * 
	 * @param carrierDictDataMap
	 * @throws DBException
	 */
	public void insertBatchCarrierDictData(ArrayList<LinkedHashMap<String, String>> carrierDictDataMap)
			throws DBException {
		logger.info("Inserting records in opsbicarrierdict");
		final int batchSize = 100000;
		int count = 0;
		try {
			conn.setAutoCommit(false);
			for (LinkedHashMap<String, String> eachRec : carrierDictDataMap) {
				insertCarrierDictDataPS.setString(1, eachRec.get("pack_id").toString());
				insertCarrierDictDataPS.setString(2, eachRec.get("carriervalue").toString());
				insertCarrierDictDataPS.setString(3, eachRec.get("carriervaluestd").toString());

				insertCarrierDictDataPS.addBatch();
				if (++count % batchSize == 0) {
					logger.info("Carrier Dict count=" + count + ",batchSize=" + batchSize);
					insertCarrierDictDataPS.executeBatch();
					conn.commit();
				}
			}
			insertCarrierDictDataPS.executeBatch();// Remaining records
			conn.commit();
			logger.info("Completed updating Carrier Dictionary Cube Data");

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in inserting Carrier Data in opsbicarrierdict Table");
			throw new DBException("In insertBatchCarrierDictData()", e);
		}
	}

}
