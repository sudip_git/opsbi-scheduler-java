/**
 * 
 */
package com.sdsl.opsbi.metric;

/**
 * @author Sudip Das
 *
 */
public interface IMetric {

	/**
	 * Metric Volume
	 */
	public final static int FILES = 1;
	public final static int PACKS = 2;
	public final static int EXTRACTS = 3;
	public final static int PAGES = 4;

	/**
	 * Metric Volume-Against
	 */
	public final static int LASTDAY = 1;
	public final static int ONEWEEK = 2;
	public final static int PREVIOUSMONTH = 3;
	public final static int CURRENTMONTH = 4;
	public final static int THREEMONTHS = 5;
	public final static int SIXMONTHS = 6;
	public final static int TWELVEMONTHS = 7;

	/**
	 * Metric Volume=Against-GroupBy
	 */
	public final static int NO_GROUPBY = 0;
	public final static int LOB = 1;
	public final static int CARRIER = 2;
	public final static int CLIENT = 3;
	public final static int REGION = 4;

}
