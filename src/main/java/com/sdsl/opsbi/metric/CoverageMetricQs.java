/**
 * 
 */
package com.sdsl.opsbi.metric;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.sdsl.opsbi.OpsBiScheduler;
import com.sdsl.opsbi.db.DBController;
import com.sdsl.opsbi.exceptions.DBException;
import com.sdsl.opsbi.utils.Configurator;
import com.sdsl.opsbi.utils.S3BucketHandler;

/**
 * @author Sudip Das
 *
 */
public class CoverageMetricQs implements IMetric {

	public static List<String> coverageQsList = new ArrayList<String>();

	public int getRandomInteger(int maximum, int minimum) {
		return ((int) (Math.random() * (maximum - minimum))) + minimum;
	}

	public int[] getNonRepeatingRandomIntegers(int returnArraySize, int maxRange) {
		assert returnArraySize <= maxRange : "cannot get more unique numbers than the size of the range";
		int[] result = new int[returnArraySize];
		Set<Integer> used = new HashSet<Integer>();

		for (int i = 0; i < returnArraySize; i++) {

			int newRandom;
			do {
				newRandom = new Random().nextInt(maxRange + 1);
			} while (used.contains(newRandom));
			result[i] = newRandom;
			used.add(newRandom);
		}
//		for (int j = 0; j < result.length; j++) {
//			System.err.println(result[j]);
//		}
		return result;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ArrayList<LinkedHashMap<String, Object>> getJsonDataQsCoverageVsLob(DBController controller)
			throws DBException {

		ArrayList<LinkedHashMap<String, Object>> forJson = new ArrayList<LinkedHashMap<String, Object>>();

		Multimap<String, LinkedHashMap<String, Object>> coverageDatarecords = controller.getMetricCoverageRecs();

		for (String eachQsKey : coverageDatarecords.keySet()) {
			LinkedHashMap<String, Object> currentMap = new LinkedHashMap<String, Object>();
			ArrayList<LinkedHashMap<String, Object>> topTenLobsForSeriesList = new ArrayList<LinkedHashMap<String, Object>>();
			Multimap<String, Double> othersLobMultiMap = ArrayListMultimap.create();
			Collection collection = coverageDatarecords.get(eachQsKey);
			Iterator<LinkedHashMap<String, Object>> iterator = collection.iterator();
			while (iterator.hasNext()) {
				LinkedHashMap<String, Object> currentSeriesMap = iterator.next();
				/*
				 * Add only if name has top ten lobs else add to non top ten Multimap;
				 */
//				if (OpsBiScheduler.getTopLobs().contains(currentSeriesMap.get("name"))) 
				if (OpsBiScheduler.getTopLobs().stream()
						.anyMatch(currentSeriesMap.get("name").toString()::equalsIgnoreCase)) {
					topTenLobsForSeriesList.add(currentSeriesMap);
				} else {
					othersLobMultiMap.put("OTHERS", (Double) currentSeriesMap.get("value"));
				}
			}
			/*
			 * Restricting to top ten lobs. Check if Others exist. first else it will be NaN
			 */
			List<Double> othersLobValuesList = (List<Double>) othersLobMultiMap.get("OTHERS");
			if (othersLobValuesList.size() > 0) {
				double sumOthersLobValues = othersLobValuesList.stream().mapToDouble(Double::doubleValue).sum();
				double avgOthersLobValues = sumOthersLobValues / othersLobValuesList.size();
				avgOthersLobValues = Math.round(avgOthersLobValues * 100.0) / 100.0;
//				double avgOthersLobValues = 0;
//				avgOthersLobValues = othersLobValuesList.size() == 0 ? avgOthersLobValues
//						: sumOthersLobValues / othersLobValuesList.size();

				/*
				 * Add a map with key as others for the other Lobs and finally add to the
				 * toptenLobs Series
				 */
				LinkedHashMap<String, Object> otherLobsMap = new LinkedHashMap<String, Object>();
				otherLobsMap.put("name", "OTHERS");
				otherLobsMap.put("value", avgOthersLobValues);
				topTenLobsForSeriesList.add(otherLobsMap);
			}

			currentMap.put("name", eachQsKey);
			currentMap.put("series", topTenLobsForSeriesList);
			forJson.add(currentMap);
		}
		return forJson;
	}

	public String getChartName(int charType) {

		switch (charType) {
		case OpsBiScheduler.LINE_CHART:
			return "line";
		case OpsBiScheduler.HEAT_MAP:
			return "heatmap";
		case OpsBiScheduler.BAR_CHART:
			return "bar";
		case OpsBiScheduler.PIE_CHART:
			return "pie";
		case OpsBiScheduler.TREEMAP:
			return "treemap";
		case OpsBiScheduler.NUMBER_CARD:
			return "numbercard";
		case OpsBiScheduler.GAUGE:
			return "gauge";
		case OpsBiScheduler.BUBBLE_CHART:
			return "bubble";
		case OpsBiScheduler.DEPENDENCY_ECHART:
			return "dependencygraph";
		default:
			return null;
		}
	}

	public String getAgainstName(int againstType) {

		switch (againstType) {
		case IMetric.LOB:
			return "lob";
		case IMetric.CARRIER:
			return "carrier";
		case IMetric.CLIENT:
			return "client";
		case IMetric.REGION:
			return "region";
		default:
			return null;
		}
	}

	public static int getAgainstCodeForAgainstStr(String againstStr) {
		againstStr = againstStr.trim();
		if (againstStr.equalsIgnoreCase("lob")) {
			return IMetric.LOB;
		} else if (againstStr.equalsIgnoreCase("carrier")) {// Not implemented
			return IMetric.CARRIER;
		} else if (againstStr.equalsIgnoreCase("client")) {// Not implemented
			return IMetric.CLIENT;
		} else if (againstStr.equalsIgnoreCase("region")) {// Not implemented
			return IMetric.REGION;
		} else {
			return -1;
		}
	}

	@SuppressWarnings("unchecked")
	public void generatePieTreeCardGaugeCoverageJsons(DBController controller, int chartType, int againstType)
			throws DBException {

		CoverageMetricQs coverageMetricQs = new CoverageMetricQs();
		coverageMetricQs.getAgainstName(againstType);

		ArrayList<LinkedHashMap<String, Object>> forJson = null;
		switch (againstType) {
		case IMetric.LOB:
			forJson = coverageMetricQs.getJsonDataQsCoverageVsLob(controller);
			break;
		case IMetric.CARRIER:
			break;
		case IMetric.CLIENT:
			break;
		case IMetric.REGION:
			break;
		}

//		ArrayList<LinkedHashMap<String, Object>> forJson = coverageMetricQs.getJsonDataQsCoverageVsLob(controller);

		for (LinkedHashMap<String, Object> eachQsJsonMap : forJson) {
			// Check if this is configured question. The qs_key is in "name", value in
			// "series"
			if (coverageQsList.contains(eachQsJsonMap.get("name"))) {
				String metricQsName = eachQsJsonMap.get("name").toString();
				ArrayList<LinkedHashMap<String, Object>> seriesList = (ArrayList<LinkedHashMap<String, Object>>) eachQsJsonMap
						.get("series");
				String jsonStr = new Gson().toJson(seriesList);
				String jsonFileName = coverageMetricQs.getChartName(chartType) + "_coverage_" + metricQsName + "_"
						+ coverageMetricQs.getAgainstName(againstType) + ".json";
				S3BucketHandler.uploadContentToS3(jsonStr, jsonFileName, Configurator.DATA_BUCKET);
			}
		}
	}

	public void generateLineHeatmapCoverageJsons(DBController controller, int chartType, int againstType)
			throws DBException {

		CoverageMetricQs coverageMetricQs = new CoverageMetricQs();
		coverageMetricQs.getAgainstName(againstType);

		ArrayList<LinkedHashMap<String, Object>> forJson = null;
		switch (againstType) {
		case IMetric.LOB:
			forJson = coverageMetricQs.getJsonDataQsCoverageVsLob(controller);
			break;
		case IMetric.CARRIER:
			break;
		case IMetric.CLIENT:
			break;
		case IMetric.REGION:
			break;
		}

		for (LinkedHashMap<String, Object> eachQsJsonMap : forJson) {
			// Check if this is configured question. The qs_key is in "name", value in
			// "series"
			if (coverageQsList.contains(eachQsJsonMap.get("name"))) {
				ArrayList<LinkedHashMap<String, Object>> modifiedForJson = new ArrayList<LinkedHashMap<String, Object>>();
				String metricQsName = eachQsJsonMap.get("name").toString();
				/*
				 * Initially the Map contains qs key as name. qs_key is replaced by
				 * "Coverage Percentage" as per the json format
				 */
				eachQsJsonMap.put("name", "Coverage Percentage");
				modifiedForJson.add(eachQsJsonMap);
				String jsonStr = new Gson().toJson(modifiedForJson);
//				System.err.println(jsonStr);
				String jsonFileName = coverageMetricQs.getChartName(chartType) + "_coverage_" + metricQsName + "_"
						+ coverageMetricQs.getAgainstName(againstType) + ".json";
				S3BucketHandler.uploadContentToS3(jsonStr, jsonFileName, Configurator.DATA_BUCKET);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void generateBubbleCoverageJsons(DBController controller, int chartType, int againstType)
			throws DBException {

		CoverageMetricQs coverageMetricQs = new CoverageMetricQs();
		coverageMetricQs.getAgainstName(againstType);

		ArrayList<LinkedHashMap<String, Object>> forJson = null;
		switch (againstType) {
		case IMetric.LOB:
			forJson = coverageMetricQs.getJsonDataQsCoverageVsLob(controller);
			break;
		case IMetric.CARRIER:
			break;
		case IMetric.CLIENT:
			break;
		case IMetric.REGION:
			break;
		}

//		ArrayList<LinkedHashMap<String, Object>> forJson = coverageMetricQs.getJsonDataQsCoverageVsLob(controller);

		for (LinkedHashMap<String, Object> eachQsJsonMap : forJson) {
			// Check if this is configured question. The qs_key is in "name", value in
			// "series"
			if (coverageQsList.contains(eachQsJsonMap.get("name"))) {
				ArrayList<LinkedHashMap<String, Object>> modifiedForJson = new ArrayList<LinkedHashMap<String, Object>>();
				LinkedHashMap<String, Object> newRootNode = new LinkedHashMap<String, Object>();
				newRootNode.put("name", "Coverage Percentage");
				String metricQsName = eachQsJsonMap.get("name").toString();
				ArrayList<LinkedHashMap<String, Object>> currentSeriesList = (ArrayList<LinkedHashMap<String, Object>>) eachQsJsonMap
						.get("series");
				ArrayList<LinkedHashMap<String, Object>> refinedSeriesList = new ArrayList<LinkedHashMap<String, Object>>();
				double maxValue = 0.0;
				double minValue = 0.0;
				for (LinkedHashMap<String, Object> eachSeriesMap : currentSeriesList) {
					LinkedHashMap<String, Object> tempSeriesMap = new LinkedHashMap<String, Object>();
					tempSeriesMap.put("name", eachSeriesMap.get("name").toString());
					tempSeriesMap.put("x", eachSeriesMap.get("name").toString());
					tempSeriesMap.put("y", eachSeriesMap.get("value"));
					double radius = Double.parseDouble(eachSeriesMap.get("value").toString());
					radius = Math.sqrt(radius);
					radius = Math.round(radius * 100.0) / 100.0;

					if (radius > maxValue) {
						maxValue = radius;
					}
					minValue = radius;
					minValue = radius < minValue ? radius : minValue;

					tempSeriesMap.put("r", radius);
					refinedSeriesList.add(tempSeriesMap);
					newRootNode.put("series", refinedSeriesList);

				}

				modifiedForJson.add(newRootNode);
				String jsonStr = new Gson().toJson(modifiedForJson);
				String jsonFileName = coverageMetricQs.getChartName(chartType) + "_coverage_" + metricQsName + "_"
						+ coverageMetricQs.getAgainstName(againstType) + ".json";
//				System.out.println(jsonFileName);
//				System.err.println(jsonStr);
				S3BucketHandler.uploadContentToS3(jsonStr, jsonFileName, Configurator.DATA_BUCKET);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void generateEchartDependencyGraph(DBController controller, int chartType, int againstType)
			throws DBException {
		CoverageMetricQs coverageMetricQs = new CoverageMetricQs();
		coverageMetricQs.getAgainstName(againstType);

		ArrayList<LinkedHashMap<String, Object>> forJson = null;
		switch (againstType) {
		case IMetric.LOB:
			forJson = coverageMetricQs.getJsonDataQsCoverageVsLob(controller);
			break;
		case IMetric.CARRIER:
			break;
		case IMetric.CLIENT:
			break;
		case IMetric.REGION:
			break;
		}

		LinkedHashMap<String, ArrayList<LinkedHashMap<String, Object>>> modifiedForJson = new LinkedHashMap<String, ArrayList<LinkedHashMap<String, Object>>>();
		ArrayList<LinkedHashMap<String, Object>> nodesList = new ArrayList<LinkedHashMap<String, Object>>();
		ArrayList<LinkedHashMap<String, Object>> edgesList = new ArrayList<LinkedHashMap<String, Object>>();
		for (LinkedHashMap<String, Object> eachQsJsonMap : forJson) {
//			System.err.println(eachQsJsonMap);
			if (coverageQsList.contains(eachQsJsonMap.get("name"))) {
				LinkedHashMap<String, Object> currentNodeMap = new LinkedHashMap<String, Object>();

				/*
				 * nodesSizeAttr provides average coverage of the question for all the LOBs
				 * present as records
				 * 
				 */
				double nodesSizeAttr = 0.0;

				String metricQsName = eachQsJsonMap.get("name").toString();
				currentNodeMap.put("id", metricQsName);
				// Size nodesSizeAttr is populated at the end before adding to nodesList

				ArrayList<LinkedHashMap<String, Object>> currentSeriesList = (ArrayList<LinkedHashMap<String, Object>>) eachQsJsonMap
						.get("series");

				ArrayList<Double> allSeriesValues = new ArrayList<Double>();
				for (LinkedHashMap<String, Object> eachSeriesMap : currentSeriesList) {
					/*
					 * edgesSizeAttrib provides value of coverage of a question in one Lob under
					 * consideration
					 * 
					 */
					double edgesSizeAttrib = (Double) eachSeriesMap.get("value");
					edgesSizeAttrib = Math.round(edgesSizeAttrib * 100.0) / 100.0;// Rounding to two decimal places

					if (edgesSizeAttrib > nodesSizeAttr) {
						nodesSizeAttr = edgesSizeAttrib;
					}

					allSeriesValues.add(edgesSizeAttrib);
				}
				/*
				 * Provisioning size attribute for nodesList by taking average across the LOBs
				 */
//				nodesSizeAttr = Math.round(nodesSizeAttr * 100.0) / 100.0;
				currentNodeMap.put("size", nodesSizeAttr);
				nodesList.add(currentNodeMap);

				/*
				 * Reverse sort to get descending order and the keep the top 3. Ensure that
				 * there are at least 3 or more values in the original series.
				 */

				Collections.sort(allSeriesValues, Collections.reverseOrder());
				ArrayList<Double> finalSeriesValuesConsidered = null;
				if (allSeriesValues.size() >= 3) {
					finalSeriesValuesConsidered = new ArrayList<Double>();
					finalSeriesValuesConsidered.add(allSeriesValues.get(0));
					finalSeriesValuesConsidered.add(allSeriesValues.get(1));
					finalSeriesValuesConsidered.add(allSeriesValues.get(2));
				} else {
					finalSeriesValuesConsidered = allSeriesValues;
				}

				for (LinkedHashMap<String, Object> eachSeriesMap : currentSeriesList) {
					LinkedHashMap<String, Object> currentEdgesMap = new LinkedHashMap<String, Object>();
					double edgesSizeAttrib = (Double) eachSeriesMap.get("value");
					edgesSizeAttrib = Math.round(edgesSizeAttrib * 100.0) / 100.0;// Rounding to two decimal places

					if (finalSeriesValuesConsidered.contains(edgesSizeAttrib)) {
						String lobName = eachSeriesMap.get("name").toString();

						currentEdgesMap.put("sourceID", metricQsName);
						currentEdgesMap.put("targetID", lobName);
						currentEdgesMap.put("size", edgesSizeAttrib);
						edgesList.add(currentEdgesMap);
					}
				}
			}
		} // seriesLobValuesMMap

		// Add LOB names to nodesList
		for (String eachLob : OpsBiScheduler.getTopLobs()) {
			LinkedHashMap<String, Object> currentLobNodeMap = new LinkedHashMap<String, Object>();
			currentLobNodeMap.put("id", eachLob);
			currentLobNodeMap.put("size", 1.0);
			nodesList.add(currentLobNodeMap);
		}

//		System.err.println("nodesList:" + nodesList);
//		System.err.println("edgesList:" + edgesList);
		modifiedForJson.put("nodes", nodesList);
		modifiedForJson.put("edges", edgesList);

		String jsonStr = new Gson().toJson(modifiedForJson);
//		System.err.println("jsonStr:" + jsonStr);// dependencygraph_coverage_PP1_lob
		String jsonFileName = coverageMetricQs.getChartName(chartType) + "_coverage_" + "all" + "_"
				+ coverageMetricQs.getAgainstName(againstType) + ".json";
//		System.out.println(jsonFileName);
//		System.err.println(jsonStr);
		S3BucketHandler.uploadContentToS3(jsonStr, jsonFileName, Configurator.DATA_BUCKET);
	}

	@SuppressWarnings("unchecked")
	public void generateEchartBarLineCoverageJsons(DBController controller, int chartType, int againstType)
			throws DBException {

		CoverageMetricQs coverageMetricQs = new CoverageMetricQs();
		coverageMetricQs.getAgainstName(againstType);

		ArrayList<LinkedHashMap<String, Object>> forJson = null;
		switch (againstType) {
		case IMetric.LOB:
			forJson = coverageMetricQs.getJsonDataQsCoverageVsLob(controller);
			break;
		case IMetric.CARRIER:
			break;
		case IMetric.CLIENT:
			break;
		case IMetric.REGION:
			break;
		}

		for (LinkedHashMap<String, Object> eachQsJsonMap : forJson) {
			LinkedHashMap<String, Object> refinedForJson = new LinkedHashMap<String, Object>();

			LinkedHashMap<String, Object> xAxisComponentsMap = new LinkedHashMap<String, Object>();
			LinkedHashMap<String, Object> yAxisComponentsMap = new LinkedHashMap<String, Object>();
			LinkedHashMap<String, Object> seriesComponentsMap = new LinkedHashMap<String, Object>();
			ArrayList<LinkedHashMap<String, Object>> allSeriesDataList = new ArrayList<LinkedHashMap<String, Object>>();
			// Check if this is configured question. The qs_key is in "name", value in
			// "series"
			if (coverageQsList.contains(eachQsJsonMap.get("name"))) {
				String metricQsName = eachQsJsonMap.get("name").toString();
				ArrayList<LinkedHashMap<String, Object>> seriesList = (ArrayList<LinkedHashMap<String, Object>>) eachQsJsonMap
						.get("series");
				ArrayList<String> xAxisDataList = new ArrayList<String>();
				ArrayList<Double> seriesDataList = new ArrayList<Double>();

				for (LinkedHashMap<String, Object> eachSeriesMap : seriesList) {
					xAxisDataList.add(eachSeriesMap.get("name").toString());
					seriesDataList.add((Double) eachSeriesMap.get("value"));
				}

				xAxisComponentsMap.put("type", "category");
				xAxisComponentsMap.put("data", xAxisDataList);
				yAxisComponentsMap.put("type", "value");
				seriesComponentsMap.put("data", seriesDataList);
				allSeriesDataList.add(seriesComponentsMap);

				refinedForJson.put("xAxis", xAxisComponentsMap);
				refinedForJson.put("yAxis", yAxisComponentsMap);
				refinedForJson.put("series", allSeriesDataList);

				String jsonStr = new Gson().toJson(refinedForJson);
				String jsonFileName = coverageMetricQs.getChartName(chartType) + "_coverage_" + metricQsName + "_"
						+ coverageMetricQs.getAgainstName(againstType) + ".json";
				S3BucketHandler.uploadContentToS3(jsonStr, jsonFileName, Configurator.DATA_BUCKET);
			}
		}
	}

	/*
	 * Inner Circle: Single record and Multiple records Second Inner Circle: Single
	 * record children: UC1, PP1, PN1, FN1 Second Inner Circle: Multiple records
	 * children: NI1, NS1, MA1 and the rest Third circle: randomized LOBs between 2
	 * to 10 Fourth circle: percentage coverage for that LOB
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public void generateEchartSunburst(DBController controller, int chartType, int againstType) throws DBException {

		getAgainstName(againstType);

		ArrayList<LinkedHashMap<String, Object>> forJson = null;
		switch (againstType) {
		case IMetric.LOB:
			forJson = getJsonDataQsCoverageVsLob(controller);
			break;
		case IMetric.CARRIER:
			break;
		case IMetric.CLIENT:
			break;
		case IMetric.REGION:
			break;
		}
		ArrayList<String> singlerRecordQs = new ArrayList<String>();
		singlerRecordQs.add("UC1");
		singlerRecordQs.add("PP1");
		singlerRecordQs.add("PN1");
		singlerRecordQs.add("FN1");
//		System.err.println(singlerRecordQs);

		ArrayList<String> multiRecordsQs = new ArrayList<String>();
		for (String eachQs : coverageQsList) {
			if (!singlerRecordQs.stream().anyMatch(eachQs::equalsIgnoreCase)) {
				multiRecordsQs.add(eachQs);
			}
		}
//		System.err.println(multiRecordsQs);

		// Deal with single records first

		ArrayList<LinkedHashMap<String, Object>> firstCircle = new ArrayList<LinkedHashMap<String, Object>>();
		LinkedHashMap<String, Object> singleLineCircle = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> multiLineCircle = new LinkedHashMap<String, Object>();

		ArrayList<LinkedHashMap<String, Object>> consideredSlRecMaps = new ArrayList<LinkedHashMap<String, Object>>();

		for (LinkedHashMap<String, Object> eachQsJsonMap : forJson) {
			LinkedHashMap<String, Object> thirdCircle = new LinkedHashMap<String, Object>();
//			System.err.println(eachQsJsonMap);
			String metricQsName = eachQsJsonMap.get("name").toString();
			if (singlerRecordQs.stream().anyMatch(metricQsName::equalsIgnoreCase)) {
				ArrayList<LinkedHashMap<String, Object>> seriesList = (ArrayList<LinkedHashMap<String, Object>>) eachQsJsonMap
						.get("series");
				// random number of lobs considered between 1 and 4
				int maxLobsConsidered = getRandomInteger(1, 5);// between 1 and 4
				/*
				 * instead of taking the lobs as per the sequence they arrive, the indices of
				 * lobs considered is randomized. First step: Randomize the number of lobs to
				 * pick (number between 1 and 4). Second Step: Based on the first step randomize
				 * the indices number. Example if first step gives 4, second step will give 4
				 * random indices lying between 1 and 10
				 */
				int numberofIndices = getRandomInteger(1, 6);// between 1 and 4
				int[] randomIndices = getNonRepeatingRandomIntegers(numberofIndices, 9);// between 1 and 10
				int count = 1;
//				System.out.println("Max Lobs for =" + metricQsName + ":" + maxLobsConsidered);
//				System.out.println("numberofIndices =" + metricQsName + ":" + numberofIndices);

//				System.err.println(metricQsName + ":" + seriesList);
				ArrayList<LinkedHashMap<String, Object>> consideredLobsNameValueMaps = new ArrayList<LinkedHashMap<String, Object>>();

				for (int i = 0; i < randomIndices.length; i++) {
					ArrayList<LinkedHashMap<String, Object>> refinedLowestLevelList = new ArrayList<LinkedHashMap<String, Object>>();
					LinkedHashMap<String, Object> refinedSeriesMap = new LinkedHashMap<String, Object>();
					LinkedHashMap<String, Object> randomSeriesMap = seriesList.get(randomIndices[i]);
					refinedSeriesMap.put("name", randomSeriesMap.get("name"));
					refinedSeriesMap.put("value", (Double) randomSeriesMap.get("value"));
					consideredLobsNameValueMaps.add(refinedSeriesMap);
				}
				thirdCircle.put("name", metricQsName);
				thirdCircle.put("value", consideredLobsNameValueMaps.size());
				thirdCircle.put("children", consideredLobsNameValueMaps);
			}
			if (thirdCircle.size() > 0) {
				consideredSlRecMaps.add(thirdCircle);
			}
		}
		singleLineCircle.put("name", "single record");
		singleLineCircle.put("children", consideredSlRecMaps);

		ArrayList<LinkedHashMap<String, Object>> consideredMlRecMaps = new ArrayList<LinkedHashMap<String, Object>>();

		for (LinkedHashMap<String, Object> eachQsJsonMap : forJson) {
			LinkedHashMap<String, Object> thirdCircle = new LinkedHashMap<String, Object>();
//			System.err.println(eachQsJsonMap);
			String metricQsName = eachQsJsonMap.get("name").toString();
			if (multiRecordsQs.stream().anyMatch(metricQsName::equalsIgnoreCase)) {
				ArrayList<LinkedHashMap<String, Object>> seriesList = (ArrayList<LinkedHashMap<String, Object>>) eachQsJsonMap
						.get("series");
				// random number of lobs considered between 1 and 4
				int maxLobsConsidered = getRandomInteger(1, 5);// between 1 and 4
				/*
				 * instead of taking the lobs as per the sequence they arrive, the indices of
				 * lobs considered is randomized. First step: Randomize the number of lobs to
				 * pick (number between 1 and 4). Second Step: Based on the first step randomize
				 * the indices number. Example if first step gives 4, second step will give 4
				 * random indices lying between 1 and 10
				 */
				int numberofIndices = getRandomInteger(1, 6);// between 1 and 4
				int[] randomIndices = getNonRepeatingRandomIntegers(numberofIndices, 9);// between 1 and 10
				int count = 1;
//				System.out.println("Max Lobs for =" + metricQsName + ":" + maxLobsConsidered);
//				System.out.println("numberofIndices =" + metricQsName + ":" + numberofIndices);

//				System.err.println(metricQsName + ":" + seriesList);
				ArrayList<LinkedHashMap<String, Object>> consideredLobsNameValueMaps = new ArrayList<LinkedHashMap<String, Object>>();

				for (int i = 0; i < randomIndices.length; i++) {
					ArrayList<LinkedHashMap<String, Object>> refinedLowestLevelList = new ArrayList<LinkedHashMap<String, Object>>();
					LinkedHashMap<String, Object> refinedSeriesMap = new LinkedHashMap<String, Object>();
					LinkedHashMap<String, Object> randomSeriesMap = seriesList.get(randomIndices[i]);
					refinedSeriesMap.put("name", randomSeriesMap.get("name"));
					refinedSeriesMap.put("value", (Double) randomSeriesMap.get("value"));
					consideredLobsNameValueMaps.add(refinedSeriesMap);
				}
				thirdCircle.put("name", metricQsName);
				thirdCircle.put("value", consideredLobsNameValueMaps.size());
				thirdCircle.put("children", consideredLobsNameValueMaps);
			}
			if (thirdCircle.size() > 0) {
				consideredMlRecMaps.add(thirdCircle);
			}
		}

		multiLineCircle.put("name", "multi record");
		multiLineCircle.put("children", consideredMlRecMaps);

		firstCircle.add(singleLineCircle);
		firstCircle.add(multiLineCircle);

//		System.err.println(firstCircle);
		String jsonStr = new Gson().toJson(firstCircle);
//		System.err.println(jsonStr);

		String jsonFileName = "sunburst_coverage_UC1_lob.json";
		S3BucketHandler.uploadContentToS3(jsonStr, jsonFileName, Configurator.DATA_BUCKET);
	}
}
