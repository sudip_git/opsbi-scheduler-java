/**
 * 
 */
package com.sdsl.opsbi.metric;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.javatuples.Pair;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.sdsl.opsbi.OpsBiScheduler;
import com.sdsl.opsbi.db.DBController;
import com.sdsl.opsbi.exceptions.DBException;

/**
 * @author Sudip Das
 *
 */
public class VolumeMetric implements IMetric {

	public static int getMetricOfIdForMetricName(String metricStr) {
		metricStr = metricStr.trim();
		if (metricStr.equalsIgnoreCase("files")) {
			return IMetric.FILES;
		} else if (metricStr.equalsIgnoreCase("packs")) {
			return IMetric.PACKS;
		} else if (metricStr.equalsIgnoreCase("extracts")) {
			return IMetric.EXTRACTS;
		} else if (metricStr.equalsIgnoreCase("pages")) {
			return IMetric.PAGES;
		} else {
			return -1;
		}
	}

	/**
	 * @param againstStr
	 * @return
	 */
	public static int getAgainstCodeForAgainstStr(String againstStr) {
		againstStr = againstStr.trim();
		if (againstStr.equalsIgnoreCase("lastday")) {
			return IMetric.LASTDAY;
		} else if (againstStr.equalsIgnoreCase("oneweek")) {
			return IMetric.ONEWEEK;
		} else if (againstStr.equalsIgnoreCase("previousmonth")) {
			return IMetric.PREVIOUSMONTH;
		} else if (againstStr.equalsIgnoreCase("currentmonth")) {
			return IMetric.CURRENTMONTH;
		} else if (againstStr.equalsIgnoreCase("threemonths")) {
			return IMetric.THREEMONTHS;
		} else if (againstStr.equalsIgnoreCase("sixmonths")) {
			return IMetric.SIXMONTHS;
		} else if (againstStr.equalsIgnoreCase("twelvemonths")) {
			return IMetric.TWELVEMONTHS;
		} else {
			return -1;
		}
	}

	/**
	 * @param stackByStr
	 * @return
	 */
	public static int getStackByCodeForStackByStr(String stackByStr) {
		stackByStr = stackByStr.trim();
		if (stackByStr.equalsIgnoreCase("lob")) {
			return IMetric.LOB;
		} else if (stackByStr.equalsIgnoreCase("carrier")) {// Not implemented
			return IMetric.CARRIER;
		} else if (stackByStr.equalsIgnoreCase("client")) {// Not implemented
			return IMetric.CLIENT;
		} else if (stackByStr.equalsIgnoreCase("region")) {// Not implemented
			return IMetric.REGION;
		} else {
			return -1;
		}
	}

	/**
	 * @param date
	 * @param caseType
	 * @return
	 */
	public static ArrayList<Pair<Timestamp, Timestamp>> getTimeStampIntervalsList(Date date, int caseType) {
		ArrayList<Pair<Timestamp, Timestamp>> startEndTsList = new ArrayList<Pair<Timestamp, Timestamp>>();

//		DateTime currDateTime = new DateTime(date);

		switch (caseType) {
		case IMetric.ONEWEEK:// WEEK DATES CALCULATION
			startEndTsList = getTimestampsForMultiDays(date, 7);
//			System.err.println(startEndTsList);
			break;
		case IMetric.CURRENTMONTH:// WEEK DATES CALCULATION
			startEndTsList = getTimestampsForMultiWeeks(date, 5);
//			System.err.println(startEndTsList);
			break;
		case IMetric.PREVIOUSMONTH:// MONTH DATES CALCULATION
			/*
			 * Moves cursor one month back and then generates current time to get max value
			 * of the month
			 */
			DateTime lastMonthDt = new DateTime(date).minusMonths(1);
			date = lastMonthDt.toDate();
			startEndTsList = getTimestampsForMultiWeeks(date, 5);
//			System.err.println(startEndTsList);
			break;
		case IMetric.THREEMONTHS:// QUARTER DATES CALCULATION
			startEndTsList = getTimestampsForMultiMonths(date, 3);
//			System.err.println(startEndTsList);
			break;
		case IMetric.SIXMONTHS:// 6 MONTHS HALF-YEAR DATES CALCULATION
			startEndTsList = getTimestampsForMultiMonths(date, 6);
//			System.err.println(startEndTsList);
			break;
		case IMetric.TWELVEMONTHS:// YEAR DATES CALCULATION
			startEndTsList = getTimestampsForMultiMonths(date, 12);
//			System.err.println(startEndTsList);
			break;
		}

		return startEndTsList;
	}

	/**
	 * Getting Timestamps for days of week
	 * 
	 * @param currDate
	 * @param numOfDays
	 * @return
	 */
	private static ArrayList<Pair<Timestamp, Timestamp>> getTimestampsForMultiDays(Date currDate, int numOfDays) {
		ArrayList<Pair<Timestamp, Timestamp>> startEndTsList = new ArrayList<Pair<Timestamp, Timestamp>>();

		LocalDate ld = new LocalDate(currDate);
		Date tempDate = ld.toDate();
		DateTime startDateTime = new DateTime(tempDate);

		for (int i = 0; i < numOfDays; i++) {
			DateTime endDateTime = startDateTime;
			startEndTsList
					.add(Pair.with(new Timestamp(startDateTime.getMillis()), new Timestamp(endDateTime.getMillis())));
			startDateTime = startDateTime.minusDays(1);
		}
		Collections.reverse(startEndTsList);
		return startEndTsList;
	}

	/**
	 * Getting Timestamps for weeks in a month
	 * 
	 * @param currDate
	 * @param numOfWeeks
	 * @return
	 */
	private static ArrayList<Pair<Timestamp, Timestamp>> getTimestampsForMultiWeeks(Date currDate, int numOfWeeks) {
		ArrayList<Pair<Timestamp, Timestamp>> startEndTsList = new ArrayList<Pair<Timestamp, Timestamp>>();
		LocalDate ld = new LocalDate(currDate);
		Date tempDate = ld.toDate();
		DateTime startDateTime = new DateTime(tempDate).withDayOfMonth(1);

		// Adding a week should not cross over to next month hence this DateTime is used
		DateTime maxMonthDaysDateTime = startDateTime.dayOfMonth().withMaximumValue();

		for (int i = 0; i < numOfWeeks; i++) {
			DateTime endDateTime = startDateTime.plusWeeks(1).minusDays(1);
			// Adding a week should not cross over to next month
			if (endDateTime.getMonthOfYear() > startDateTime.getMonthOfYear()) {
				startEndTsList.add(Pair.with(new Timestamp(startDateTime.getMillis()),
						new Timestamp(maxMonthDaysDateTime.getMillis())));
			} else {
				startEndTsList.add(
						Pair.with(new Timestamp(startDateTime.getMillis()), new Timestamp(endDateTime.getMillis())));
			}
			startDateTime = endDateTime.plusDays(1);
		}
		return startEndTsList;
	}

	/**
	 * Getting Timestamps for months in Quarter, Half Year or Year
	 * 
	 * @param currDate
	 * @param numOfMonths
	 * @return
	 */
	private static ArrayList<Pair<Timestamp, Timestamp>> getTimestampsForMultiMonths(Date currDate, int numOfMonths) {
		ArrayList<Pair<Timestamp, Timestamp>> startEndTsList = new ArrayList<Pair<Timestamp, Timestamp>>();
		LocalDate ld = new LocalDate(currDate);
		for (int i = 0; i < numOfMonths; i++) {
			Date tempDate = ld.toDate();
			DateTime startDateTime = new DateTime(tempDate).withDayOfMonth(1);
			DateTime endDateTime = startDateTime.dayOfMonth().withMaximumValue();
			startEndTsList
					.add(Pair.with(new Timestamp(startDateTime.getMillis()), new Timestamp(endDateTime.getMillis())));
			currDate = endDateTime.toDate();
			ld = ld.minusMonths(1);
		}
		Collections.reverse(startEndTsList);
		return startEndTsList;
	}

	/**
	 * @param controller
	 * @param dateConsidered
	 * @param metricId
	 * @param against
	 * @param groupBy
	 * @param chartType
	 * @return
	 * @throws DBException
	 */
	public static String getJsonString(DBController controller, Date dateConsidered, int metricId, int against,
			int groupBy, int chartType) throws DBException {
		ArrayList<LinkedHashMap<String, Object>> forJson = new ArrayList<LinkedHashMap<String, Object>>();
		ArrayList<Pair<Timestamp, Timestamp>> startEndTsList = null;

		VolumeMetric metricObj = new VolumeMetric();

		startEndTsList = getTimeStampIntervalsList(dateConsidered, against);

		if (groupBy == IMetric.NO_GROUPBY) {
//			if (against == IMetric.LASTMONTH || against == IMetric.LASTWEEK)
			forJson = metricObj.getJsonDataNoGroupBy(controller, startEndTsList, metricId, chartType, against);
//			else
//				forJson = metricObj.getJsonDataNoGroupBy(controller, startEndTsList, metricId, chartType);
		} else {

//			if (against == IMetric.LASTMONTH || against == IMetric.LASTWEEK)
			forJson = metricObj.getJsonDataGroupBy(controller, startEndTsList, groupBy, chartType, metricId, against);
//			else
//				forJson = metricObj.getJsonDataGroupBy(controller, startEndTsList, groupBy, chartType, metricId);
		}
		return new Gson().toJson(forJson);
	}

	/**
	 * @param controller
	 * @param startEndTsList
	 * @param metricOfId
	 * @param chartType
	 * @return
	 * @throws DBException
	 */
	public ArrayList<LinkedHashMap<String, Object>> getJsonDataNoGroupBy(DBController controller,
			ArrayList<Pair<Timestamp, Timestamp>> startEndTsList, int metricOfId, int chartType, int against)
			throws DBException {
		ArrayList<LinkedHashMap<String, Object>> forJson = new ArrayList<LinkedHashMap<String, Object>>();

		for (Pair<Timestamp, Timestamp> eachStartEndTs : startEndTsList) {
			LinkedHashMap<String, Object> currentMap = new LinkedHashMap<String, Object>();
			Map<String, Integer> recordsMap = controller.getMetricVolumeRecsNoGrpBy(eachStartEndTs.getValue0(),
					eachStartEndTs.getValue1(), metricOfId);

			int totalMetricCount = 0;
			if (chartType == OpsBiScheduler.BAR_CHART || chartType == OpsBiScheduler.PIE_CHART
					|| chartType == OpsBiScheduler.TREEMAP || chartType == OpsBiScheduler.NUMBER_CARD
					|| chartType == OpsBiScheduler.GAUGE) {
				currentMap = organizeBarPieTreeCardGaugeNoGroup(eachStartEndTs, recordsMap, against);

				totalMetricCount = 0;
				for (String eachDate : recordsMap.keySet()) {
					totalMetricCount = totalMetricCount + recordsMap.get(eachDate);
				}
				currentMap.put("value", totalMetricCount);
			} else if (chartType == OpsBiScheduler.LINE_CHART || chartType == OpsBiScheduler.HEAT_MAP) {
				currentMap = organizeLineHeatBubbleNoGroup(eachStartEndTs, recordsMap, against);
			} else if (chartType == OpsBiScheduler.BUBBLE_CHART) {
				currentMap = organizeLineHeatBubbleNoGroup(eachStartEndTs, recordsMap, against);
//				System.err.println("VM:getjsonDataNoGroupBy:"+currentMap);
			}

			forJson.add(currentMap);
		}
		return forJson;
	}

	/**
	 * @param controller
	 * @param startEndTsList
	 * @param groupByKey
	 * @param chartType
	 * @param metricOfId
	 * @return
	 * @throws DBException
	 */
	public ArrayList<LinkedHashMap<String, Object>> getJsonDataGroupBy(DBController controller,
			ArrayList<Pair<Timestamp, Timestamp>> startEndTsList, int groupByKey, int chartType, int metricOfId,
			int against) throws DBException {
		ArrayList<LinkedHashMap<String, Object>> forJson = new ArrayList<LinkedHashMap<String, Object>>();

		for (Pair<Timestamp, Timestamp> eachStartEndTs : startEndTsList) {
			LinkedHashMap<String, Object> currentMap = new LinkedHashMap<String, Object>();
			Map<String, Integer> recordsMap = controller.getMetricVolumeRecsGrpByLob(eachStartEndTs.getValue0(),
					eachStartEndTs.getValue1(), metricOfId, OpsBiScheduler.getTopLobs());

			if (chartType == OpsBiScheduler.BAR_CHART) {
				currentMap = organizeBarDataGroupBy(eachStartEndTs, recordsMap, against);
			} else {
				currentMap = organizeLineHeatBubbleGroupBy(eachStartEndTs, recordsMap, against);
			}

			forJson.add(currentMap);
		}
		return forJson;
	}

	/**
	 * @param eachStartEndTs
	 * @param recordsMap
	 * @return
	 */
	public LinkedHashMap<String, Object> organizeBarPieTreeCardGaugeNoGroup(Pair<Timestamp, Timestamp> eachStartEndTs,
			Map<String, Integer> recordsMap, int against) {
		LinkedHashMap<String, Object> currentMap = new LinkedHashMap<String, Object>();

//		currentMap.put("name", LocalDate.fromDateFields(new Date(eachStartEndTs.getValue1().getTime())).toString());

		if (against == IMetric.CURRENTMONTH || against == IMetric.PREVIOUSMONTH) {
			DateTime tmpStartDate = new DateTime(eachStartEndTs.getValue0().getTime());
			DateTime tmpEndDate = new DateTime(eachStartEndTs.getValue1().getTime());
			currentMap.put("name", tmpStartDate.getDayOfMonth() + "-" + tmpEndDate.getDayOfMonth() + " "
					+ tmpStartDate.toString("MMM"));
		} else {
			currentMap.put("name", LocalDate.fromDateFields(new Date(eachStartEndTs.getValue0().getTime())).toString());
		}

		int totalMetricCount = 0;
		for (String eachDate : recordsMap.keySet()) {
			totalMetricCount = totalMetricCount + recordsMap.get(eachDate);
		}
		currentMap.put("value", totalMetricCount);

		return currentMap;
	}

	/**
	 * @param eachStartEndTs
	 * @param recordsMap
	 * @return
	 */
	public LinkedHashMap<String, Object> organizeBarDataGroupBy(Pair<Timestamp, Timestamp> eachStartEndTs,
			Map<String, Integer> recordsMap, int against) {
		LinkedHashMap<String, Object> currentMap = new LinkedHashMap<String, Object>();
		ArrayList<LinkedHashMap<String, Object>> tempforJson = new ArrayList<LinkedHashMap<String, Object>>();

		// CHANGED Here

//		currentMap.put("name", LocalDate.fromDateFields(new Date(eachStartEndTs.getValue0().getTime())).toString());

		if (against == IMetric.CURRENTMONTH || against == IMetric.PREVIOUSMONTH) {
			DateTime tmpStartDate = new DateTime(eachStartEndTs.getValue0().getTime());
			DateTime tmpEndDate = new DateTime(eachStartEndTs.getValue1().getTime());
			currentMap.put("name", tmpStartDate.getDayOfMonth() + "-" + tmpEndDate.getDayOfMonth() + " "
					+ tmpStartDate.toString("MMM"));
		} else {
			currentMap.put("name", LocalDate.fromDateFields(new Date(eachStartEndTs.getValue0().getTime())).toString());
		}

		for (String eachLobKey : recordsMap.keySet()) {
			LinkedHashMap<String, Object> nameValueMap = new LinkedHashMap<String, Object>();
			nameValueMap.put("name", eachLobKey);
			nameValueMap.put("value", recordsMap.get(eachLobKey));
			tempforJson.add(nameValueMap);
		}
		currentMap.put("series", tempforJson);

		return currentMap;
	}

	/**
	 * @param eachStartEndTs
	 * @param recordsMap
	 * @return
	 */
	public LinkedHashMap<String, Object> organizeLineHeatBubbleNoGroup(Pair<Timestamp, Timestamp> eachStartEndTs,
			Map<String, Integer> recordsMap, int against) {
		LinkedHashMap<String, Object> currentMap = new LinkedHashMap<String, Object>();
		int totalMetricCount = 0;
		for (String eachDate : recordsMap.keySet()) {
			LinkedHashMap<String, Object> nameValueMap = new LinkedHashMap<String, Object>();
			totalMetricCount = totalMetricCount + recordsMap.get(eachDate);
//			nameValueMap.put("name", eachDate);

			if (against == IMetric.CURRENTMONTH || against == IMetric.PREVIOUSMONTH) {
				DateTime tmpStartDate = new DateTime(eachStartEndTs.getValue0().getTime());
				DateTime tmpEndDate = new DateTime(eachStartEndTs.getValue1().getTime());
				nameValueMap.put("name", tmpStartDate.getDayOfMonth() + "-" + tmpEndDate.getDayOfMonth() + " "
						+ tmpStartDate.toString("MMM"));
			} else {
				nameValueMap.put("name",
						LocalDate.fromDateFields(new Date(eachStartEndTs.getValue0().getTime())).toString());
			}

			nameValueMap.put("value", totalMetricCount);
			currentMap.put("series", nameValueMap);
		}
		return currentMap;
	}

	/**
	 * @param eachStartEndTs
	 * @param recordsMap
	 * @return
	 */
	public LinkedHashMap<String, Object> organizeLineHeatBubbleGroupBy(Pair<Timestamp, Timestamp> eachStartEndTs,
			Map<String, Integer> recordsMap, int against) {
		LinkedHashMap<String, Object> currentMap = new LinkedHashMap<String, Object>();
		ArrayList<LinkedHashMap<String, Object>> tempforJson = new ArrayList<LinkedHashMap<String, Object>>();
		for (String eachLobKey : recordsMap.keySet()) {
			LinkedHashMap<String, Object> nameValueMap = new LinkedHashMap<String, Object>();
			nameValueMap.put("name", eachLobKey);
			nameValueMap.put("value", recordsMap.get(eachLobKey));
//			nameValueMap.put("date",
//					LocalDate.fromDateFields(new Date(eachStartEndTs.getValue1().getTime())).toString());

			if (against == IMetric.CURRENTMONTH || against == IMetric.PREVIOUSMONTH) {
				DateTime tmpStartDate = new DateTime(eachStartEndTs.getValue0().getTime());
				DateTime tmpEndDate = new DateTime(eachStartEndTs.getValue1().getTime());
				nameValueMap.put("date", tmpStartDate.getDayOfMonth() + "-" + tmpEndDate.getDayOfMonth() + " "
						+ tmpStartDate.toString("MMM"));
			} else {
				nameValueMap.put("date",
						LocalDate.fromDateFields(new Date(eachStartEndTs.getValue0().getTime())).toString());
			}

			tempforJson.add(nameValueMap);
		}
		currentMap.put("series", tempforJson);
		return currentMap;
	}

	/**
	 * @param jsonStrToProcess
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	public String postprocessHeatGroupJsonString(String jsonStrToProcess) throws ParseException {

		// Reading current JSON to get the hashmaps required for further processing
		ArrayList<LinkedHashMap<String, Object>> tempList = new ArrayList<LinkedHashMap<String, Object>>();

		JSONParser jsonParser = new JSONParser();
		// Series Array
		JSONArray jsonSeriesNodeArray = (JSONArray) jsonParser.parse(jsonStrToProcess);
		Iterator<Object> seriesNodeIterator = jsonSeriesNodeArray.iterator();
		while (seriesNodeIterator.hasNext()) {
			JSONObject eachSeriesJsonObj = (JSONObject) seriesNodeIterator.next();
			// Elements within each Series Array
			JSONArray eachSeriesJsonObjElementsArray = (JSONArray) eachSeriesJsonObj.get("series");
			Iterator<Object> eachSeriesJsonObjElementsArrayIterator = eachSeriesJsonObjElementsArray.iterator();
			while (eachSeriesJsonObjElementsArrayIterator.hasNext()) {
				JSONObject eachSeriesElementInSeries = (JSONObject) eachSeriesJsonObjElementsArrayIterator.next();
				LinkedHashMap<String, Object> internalMap = new LinkedHashMap<String, Object>();
				internalMap.put("name", eachSeriesElementInSeries.get("name").toString());
				internalMap.put("value", eachSeriesElementInSeries.get("value"));
				internalMap.put("date", eachSeriesElementInSeries.get("date").toString());
				tempList.add(internalMap);
			}
		}
		// Get the top ten Lobs but remove OTHERS for heatmap
		ArrayList<String> lobList = OpsBiScheduler.getTopLobs();
		lobList.remove("OTHERS");

		// Group the maps got from original JSon and create MultiMap for Lobs
		ArrayList<LinkedHashMap<String, Object>> forJson = new ArrayList<LinkedHashMap<String, Object>>();

		TreeMap<String, Object> currentMap = new TreeMap<String, Object>();
		for (String eachLob : lobList) {
			ArrayList<LinkedHashMap<String, Object>> redefinedSeriesList = new ArrayList<LinkedHashMap<String, Object>>();
			Multimap<String, LinkedHashMap<String, Object>> tempMap = HashMultimap.create();
			for (LinkedHashMap<String, Object> eachMap : tempList) {
				if (eachMap.get("name").toString().equalsIgnoreCase(eachLob)) {
					LinkedHashMap<String, Object> subMap = new LinkedHashMap<String, Object>();
					subMap.put("name", eachMap.get("date"));
					subMap.put("value", eachMap.get("value"));
					tempMap.put("series", subMap);
					redefinedSeriesList.add(subMap);
					currentMap.put(eachLob, redefinedSeriesList);
				}
			}
		}

//		Creating the list for Json structure
		for (String eachRefinedMapKey : currentMap.keySet()) {
			LinkedHashMap<String, Object> finalRefinedMap = new LinkedHashMap<String, Object>();
			finalRefinedMap.put("name", eachRefinedMapKey);
			finalRefinedMap.put("series", currentMap.get(eachRefinedMapKey));
//			System.err.println("finalRefinedMap:"+finalRefinedMap);
			forJson.add(finalRefinedMap);
		}

		Gson gson = new Gson();
		String jsonStr = gson.toJson(forJson);
//		System.err.println("jsonStr:" + jsonStr);
		return jsonStr;
	}

	/**
	 * @param jsonStrToProcess
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	public String postprocessHeatNoGroupJsonString(String jsonStrToProcess) throws ParseException {

		ArrayList<LinkedHashMap<String, Object>> forJson = new ArrayList<LinkedHashMap<String, Object>>();
		// Reading current JSON to get the hashmaps required for further processing
		ArrayList<LinkedHashMap<String, Object>> redefinedSeriesList = new ArrayList<LinkedHashMap<String, Object>>();

		JSONParser jsonParser = new JSONParser();
		// Series Array
		JSONArray jsonSeriesNodeArray = (JSONArray) jsonParser.parse(jsonStrToProcess);
		Iterator<Object> seriesNodeIterator = jsonSeriesNodeArray.iterator();
		while (seriesNodeIterator.hasNext()) {
			JSONObject eachSeriesJsonObj = (JSONObject) seriesNodeIterator.next();
			JSONObject eacheriesElement = (JSONObject) eachSeriesJsonObj.get("series");
			LinkedHashMap<String, Object> redfinedSeriesMap = new LinkedHashMap<String, Object>();
			if (eacheriesElement != null) {// Takes care of no series element scenario
				// Here the name is date... PLEASE NOTE
				redfinedSeriesMap.put("name", eacheriesElement.get("name").toString());
				redfinedSeriesMap.put("value", eacheriesElement.get("value"));
				redefinedSeriesList.add(redfinedSeriesMap);
			}
		}
		// For creating the Json String
		LinkedHashMap<String, Object> currentMap = new LinkedHashMap<String, Object>();
		currentMap.put("name", "Total Files");
		currentMap.put("series", redefinedSeriesList);
		forJson.add(currentMap);
		Gson gson = new Gson();
		String jsonStr = gson.toJson(forJson);
		return jsonStr;
	}

	/**
	 * @param jsonStrToProcess
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	public String postprocessBubbleGroupJsonString(String jsonStrToProcess) throws ParseException {
		// Reading current JSON to get the hashmaps required for further processing
		ArrayList<LinkedHashMap<String, Object>> tempList = new ArrayList<LinkedHashMap<String, Object>>();

		JSONParser jsonParser = new JSONParser();
		// Series Array
		JSONArray jsonSeriesNodeArray = (JSONArray) jsonParser.parse(jsonStrToProcess);
		Iterator<Object> seriesNodeIterator = jsonSeriesNodeArray.iterator();
		while (seriesNodeIterator.hasNext()) {
			JSONObject eachSeriesJsonObj = (JSONObject) seriesNodeIterator.next();
			// Elements within each Series Array
			JSONArray eachSeriesJsonObjElementsArray = (JSONArray) eachSeriesJsonObj.get("series");
			Iterator<Object> eachSeriesJsonObjElementsArrayIterator = eachSeriesJsonObjElementsArray.iterator();
			while (eachSeriesJsonObjElementsArrayIterator.hasNext()) {
				JSONObject eachSeriesElementInSeries = (JSONObject) eachSeriesJsonObjElementsArrayIterator.next();
				LinkedHashMap<String, Object> internalMap = new LinkedHashMap<String, Object>();
				internalMap.put("name", eachSeriesElementInSeries.get("name").toString());
				internalMap.put("value", eachSeriesElementInSeries.get("value"));
				internalMap.put("date", eachSeriesElementInSeries.get("date").toString());
				tempList.add(internalMap);
			}
		}

		// Get the top ten Lobs
		ArrayList<String> lobList = OpsBiScheduler.getTopLobs();
		// Group the maps got from original JSon and create MultiMap for Lobs
		ArrayList<LinkedHashMap<String, Object>> forJson = new ArrayList<LinkedHashMap<String, Object>>();

		TreeMap<String, Object> currentMap = new TreeMap<String, Object>();
		for (String eachLob : lobList) {
			ArrayList<LinkedHashMap<String, Object>> redefinedSeriesList = new ArrayList<LinkedHashMap<String, Object>>();
			Multimap<String, LinkedHashMap<String, Object>> tempMap = HashMultimap.create();
			for (LinkedHashMap<String, Object> eachMap : tempList) {
				if (eachMap.get("name").toString().equalsIgnoreCase(eachLob)) {
					LinkedHashMap<String, Object> subMap = new LinkedHashMap<String, Object>();
					subMap.put("name", eachMap.get("date"));
					subMap.put("x", eachMap.get("date"));
					subMap.put("y", eachMap.get("value"));
					double count = Double.parseDouble(eachMap.get("value").toString());
					count = Math.sqrt(count) * 15.0;// Increase by 15px
					// 2 decimal places. Need to use 100.0 to get double else will give int
					count = Math.round(count * 100.0) / 100.0;
					subMap.put("r", count);
					tempMap.put("series", subMap);
					redefinedSeriesList.add(subMap);
					currentMap.put(eachLob, redefinedSeriesList);
				}
			}
		}
		
//		Creating the list for Json structure
		for (String eachRefinedMapKey : currentMap.keySet()) {
			LinkedHashMap<String, Object> finalRefinedMap = new LinkedHashMap<String, Object>();
			finalRefinedMap.put("name", eachRefinedMapKey);
			finalRefinedMap.put("series", currentMap.get(eachRefinedMapKey));
			forJson.add(finalRefinedMap);
		}
		Gson gson = new Gson();
		String jsonStr = gson.toJson(forJson);
//		System.err.println("jsonStr:" + jsonStr);
		return jsonStr;
	}

	/**
	 * @param jsonStrToProcess
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	public String postprocessBubbleNoGroupJsonString(String jsonStrToProcess) throws ParseException {

		ArrayList<LinkedHashMap<String, Object>> forJson = new ArrayList<LinkedHashMap<String, Object>>();
		// Reading current JSON to get the hashmaps required for further processing
		ArrayList<LinkedHashMap<String, Object>> redefinedSeriesList = new ArrayList<LinkedHashMap<String, Object>>();

		JSONParser jsonParser = new JSONParser();
		// Series Array
		JSONArray jsonSeriesNodeArray = (JSONArray) jsonParser.parse(jsonStrToProcess);
		Iterator<Object> seriesNodeIterator = jsonSeriesNodeArray.iterator();
		while (seriesNodeIterator.hasNext()) {
			JSONObject eachSeriesJsonObj = (JSONObject) seriesNodeIterator.next();
			JSONObject eacheriesElement = (JSONObject) eachSeriesJsonObj.get("series");
			LinkedHashMap<String, Object> redfinedSeriesMap = new LinkedHashMap<String, Object>();
			if (eacheriesElement != null) {// Takes care of no series element scenario
				// Here the name is date... PLEASE NOTE
				redfinedSeriesMap.put("name", eacheriesElement.get("name").toString());
				redfinedSeriesMap.put("x", eacheriesElement.get("name").toString());
				redfinedSeriesMap.put("y", eacheriesElement.get("value"));
				double count = Double.parseDouble(eacheriesElement.get("value").toString());
				count = Math.sqrt(count) * 15.0;// Increase by 15px
				// 2 decimal places. Need to use 100.0 to get double else will give int
				count = Math.round(count * 100.0) / 100.0;
				redfinedSeriesMap.put("r", count);
				redefinedSeriesList.add(redfinedSeriesMap);
			}
		}

		// For creating the Json String
		LinkedHashMap<String, Object> currentMap = new LinkedHashMap<String, Object>();
		currentMap.put("name", "Total Files");
		currentMap.put("series", redefinedSeriesList);
		forJson.add(currentMap);
		Gson gson = new Gson();
		String jsonStr = gson.toJson(forJson);
//		System.err.println(jsonStr);
		return jsonStr;
	}

	/**
	 * @param jsonStrToProcess
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")

	public String postprocessEchartBarNoGroupJsonString(String jsonStrToProcess) throws ParseException {
		// Reading current JSON to get the hashmaps required for further processing
		LinkedHashMap<String, Object> forJson = new LinkedHashMap<String, Object>();

		LinkedHashMap<String, Object> xAxisComponentsMap = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> yAxisComponentsMap = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> seriesComponentsMap = new LinkedHashMap<String, Object>();
		ArrayList<LinkedHashMap<String, Object>> allSeriesDataList = new ArrayList<LinkedHashMap<String, Object>>();

		JSONParser jsonParser = new JSONParser();
		// Series Array
		JSONArray jsonSeriesNodeArray = (JSONArray) jsonParser.parse(jsonStrToProcess);
		Iterator<Map<String, Object>> seriesNodeIterator = jsonSeriesNodeArray.iterator();
		ArrayList<String> xAxisData = new ArrayList<String>();
		ArrayList<Long> seriesDataList = new ArrayList<Long>();

		while (seriesNodeIterator.hasNext()) {
			Map<String, Object> currentMap = seriesNodeIterator.next();
			// Series data comes in a list of hashmaps with name as key and value as key
			xAxisData.add(currentMap.get("name").toString());
			seriesDataList.add((Long) currentMap.get("value"));
		}
		xAxisComponentsMap.put("type", "category");
		xAxisComponentsMap.put("data", xAxisData);

		yAxisComponentsMap.put("type", "value");
		seriesComponentsMap.put("data", seriesDataList);
		allSeriesDataList.add(seriesComponentsMap);

		forJson.put("xAxis", xAxisComponentsMap);
		forJson.put("yAxis", yAxisComponentsMap);
		forJson.put("series", allSeriesDataList);

		Gson gson = new Gson();
		String jsonStr = gson.toJson(forJson);
//		System.err.println(jsonStr);

		return jsonStr;
	}

	/**
	 * @param jsonStrToProcess
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings({ "unchecked" })

	public String postprocessEchartBarGroupJsonString(String jsonStrToProcess) throws ParseException {
		// Reading current JSON to get the hashmaps required for further processing
		LinkedHashMap<String, Object> forJson = new LinkedHashMap<String, Object>();

		// Reading current JSON to get the hashmaps required for further processing
		LinkedHashMap<String, Object> xAxisComponentsMap = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> yAxisComponentsMap = new LinkedHashMap<String, Object>();

		LinkedHashMap<String, Object> rawSeriesDataMaps = new LinkedHashMap<String, Object>();

		JSONParser jsonParser = new JSONParser();
		// Series Array
		JSONArray jsonNameAndSeriesArrays = (JSONArray) jsonParser.parse(jsonStrToProcess);
		Iterator<Map<String, Object>> nameSeriesIterator = jsonNameAndSeriesArrays.iterator();
		ArrayList<String> xAxisData = new ArrayList<String>();

		while (nameSeriesIterator.hasNext()) {
			/*
			 * Name key has date, series key is a list of HashMaps with lob(name) and count
			 * (value)
			 */
			Map<String, Object> currentDateWithSeriesMap = nameSeriesIterator.next();
			xAxisData.add(currentDateWithSeriesMap.get("name").toString());// Date String

			JSONArray eachSeriesJsonObjElementsArray = (JSONArray) currentDateWithSeriesMap.get("series");
			Iterator<Object> eachSeriesJsonObjElementsArrayIterator = eachSeriesJsonObjElementsArray.iterator();

			ArrayList<Object> refinedSeriesMap = new ArrayList<Object>();
			while (eachSeriesJsonObjElementsArrayIterator.hasNext()) {
				JSONObject eachSeriesElementInSeries = (JSONObject) eachSeriesJsonObjElementsArrayIterator.next();
				LinkedHashMap<String, Object> internalTmpMap = new LinkedHashMap<String, Object>();
				String currLobName = eachSeriesElementInSeries.get("name").toString();
				internalTmpMap.put("name", currLobName);
				Long currCount = (Long) eachSeriesElementInSeries.get("value");
				internalTmpMap.put("data", currCount);
				refinedSeriesMap.add(internalTmpMap);
			}
			rawSeriesDataMaps.put(currentDateWithSeriesMap.get("name").toString(), refinedSeriesMap);
		}

		/*
		 * Create a MultiMap with Key=Lob, Value=List of count for each date considered.
		 * First get the keys from initial Series Data (This is the date). For each
		 * date, get the series List. Use the top ten Lobs to group the data in the
		 * MultiMap. If the series list found is of size zero, add 0 to the count else
		 * use the actual count
		 */
		Multimap<String, Object> lobWithSeriesDataMMap = ArrayListMultimap.create();
		for (String eachDateKey : rawSeriesDataMaps.keySet()) {
			ArrayList<Object> currSeriesDataForDateKey = (ArrayList<Object>) rawSeriesDataMaps.get(eachDateKey);
			for (String eachLob : OpsBiScheduler.getTopLobs()) {
				if (currSeriesDataForDateKey.size() > 0) {
					/*
					 * When the series map is not empty, loop through the entire list, getting each
					 * map and then checking against the top lob name as key. When the name matches
					 * add the value to the corresponding element of the MultiMap.
					 */
					for (Object eachMap : currSeriesDataForDateKey) {
						LinkedHashMap<String, Object> currMap = (LinkedHashMap<String, Object>) eachMap;
						if (eachLob.equalsIgnoreCase(currMap.get("name").toString())) {// Lob name
							lobWithSeriesDataMMap.put(eachLob, currMap.get("data"));// count
						}
					}
				} else {
					/*
					 * When size is zero, add zero as count for that LOB
					 */
					lobWithSeriesDataMMap.put(eachLob, 0);
				}
			}
		}

		ArrayList<Object> refinedSeriesComponentsData = new ArrayList<Object>();
		for (String eachLobKey : lobWithSeriesDataMMap.keySet()) {
			LinkedHashMap<String, Object> tmpLobSeriesMap = new LinkedHashMap<String, Object>();
			ArrayList<Object> tmpSeriesData = new ArrayList<Object>();

			Collection<Object> currCountCol = lobWithSeriesDataMMap.get(eachLobKey);
			Iterator<Object> currCountItr = currCountCol.iterator();
			while (currCountItr.hasNext()) {
				tmpSeriesData.add(currCountItr.next());
			}
			tmpLobSeriesMap.put("name", eachLobKey);
			tmpLobSeriesMap.put("data", tmpSeriesData);
			refinedSeriesComponentsData.add(tmpLobSeriesMap);
		}

		/*
		 * Putting it all together
		 */

		xAxisComponentsMap.put("type", "category");
		xAxisComponentsMap.put("data", xAxisData);

		yAxisComponentsMap.put("type", "value");

//		
		forJson.put("xAxis", xAxisComponentsMap);
		forJson.put("yAxis", yAxisComponentsMap);
		forJson.put("series", refinedSeriesComponentsData);

		Gson gson = new Gson();
		String jsonStr = gson.toJson(forJson);

//		System.err.println("Vol Met jsonStr:" + jsonStr);

		return jsonStr;
	}

	/**
	 * @param jsonStrToProcess
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")

	public String postprocessEchartLineNoGroupJsonString(String jsonStrToProcess) throws ParseException {
		// Reading current JSON to get the hashmaps required for further processing
		LinkedHashMap<String, Object> forJson = new LinkedHashMap<String, Object>();

		LinkedHashMap<String, Object> xAxisComponentsMap = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> yAxisComponentsMap = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> seriesComponentsMap = new LinkedHashMap<String, Object>();
		ArrayList<LinkedHashMap<String, Object>> allSeriesDataList = new ArrayList<LinkedHashMap<String, Object>>();

		JSONParser jsonParser = new JSONParser();
		// Series Array example:
		// {},{},{"series":{"name":"2020-03-12","value":40}},{"series":{"name":"2020-03-11","value":1107}}
		JSONArray jsonSeriesNodeArray = (JSONArray) jsonParser.parse(jsonStrToProcess);
		Iterator<Map<String, Object>> seriesNodeIterator = jsonSeriesNodeArray.iterator();
		ArrayList<String> xAxisData = new ArrayList<String>();
		ArrayList<Long> seriesDataList = new ArrayList<Long>();

		while (seriesNodeIterator.hasNext()) {
			// Map will be like: {"series":{"name":"2020-03-11","value":1107}}
			Map<String, Object> currentSeriesMap = seriesNodeIterator.next();
			JSONObject eachSeriesJsonObjMap = (JSONObject) currentSeriesMap.get("series");
			/*
			 * Check if the JSONObject is null as some json elements can be blank. Else read
			 * the name key and the value key. Name key value has to be changed toString
			 * from Object and for Value Key cast to (Long)
			 */
//			
			if (eachSeriesJsonObjMap != null) {
				xAxisData.add(eachSeriesJsonObjMap.get("name").toString());
				seriesDataList.add((Long) eachSeriesJsonObjMap.get("value"));
			}
		}
//		System.out.println("xAxisData:" + xAxisData);
//		System.out.println("seriesDataList:" + seriesDataList);

		xAxisComponentsMap.put("type", "category");
		xAxisComponentsMap.put("data", xAxisData);

		yAxisComponentsMap.put("type", "value");
		seriesComponentsMap.put("data", seriesDataList);
		allSeriesDataList.add(seriesComponentsMap);

		forJson.put("xAxis", xAxisComponentsMap);
		forJson.put("yAxis", yAxisComponentsMap);
		forJson.put("series", allSeriesDataList);

		Gson gson = new Gson();
		String jsonStr = gson.toJson(forJson);
//		System.err.println("Generated:"+jsonStr);
		return jsonStr;
	}

	/**
	 * @param jsonStrToProcess
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")

	public String postprocessEchartLineGroupJsonString(String jsonStrToProcess) throws ParseException {

		// Reading current JSON to get the hashmaps required for further processing
		LinkedHashMap<String, Object> forJson = new LinkedHashMap<String, Object>();

		// Reading current JSON to get the hashmaps required for further processing
		LinkedHashMap<String, Object> xAxisComponentsMap = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> yAxisComponentsMap = new LinkedHashMap<String, Object>();
		ArrayList<Object> allSeriesDataList = new ArrayList<Object>();

		JSONParser jsonParser = new JSONParser();
		// Series Array example:
		// {},{},{"series":{"name":"2020-03-12","value":40},{"name":"BOP","value":330,"date":"2020-03-19"}}
		JSONArray jsonNameAndSeriesArrays = (JSONArray) jsonParser.parse(jsonStrToProcess);
		Iterator<Map<String, Object>> nameSeriesIterator = jsonNameAndSeriesArrays.iterator();
		Set<String> xAxisData = new LinkedHashSet<String>();// For correct order of insertion
		ArrayList<LinkedHashMap<String, Object>> tempList = new ArrayList<LinkedHashMap<String, Object>>();
		Set<String> lobsPresent = new HashSet<String>();
		while (nameSeriesIterator.hasNext()) {
			Map<String, Object> currentMap = nameSeriesIterator.next();
			JSONArray eachSeriesJsonObjElementsArray = (JSONArray) currentMap.get("series");
			Iterator<Object> eachSeriesJsonObjElementsArrayIterator = eachSeriesJsonObjElementsArray.iterator();
			while (eachSeriesJsonObjElementsArrayIterator.hasNext()) {
				JSONObject eachSeriesElementInSeries = (JSONObject) eachSeriesJsonObjElementsArrayIterator.next();
				LinkedHashMap<String, Object> internalMap = new LinkedHashMap<String, Object>();

				String lobName = eachSeriesElementInSeries.get("name").toString();
				internalMap.put("name", lobName);
				internalMap.put("value", eachSeriesElementInSeries.get("value"));
				tempList.add(internalMap);
				lobsPresent.add(lobName);
				xAxisData.add(eachSeriesElementInSeries.get("date").toString());
			}
		}


		for (String eachLob : lobsPresent) {
			LinkedHashMap<String, Object> seriesLobMap = new LinkedHashMap<String, Object>();
			seriesLobMap.put("name", eachLob);
			ArrayList<Long> currentSeriesDataList = new ArrayList<Long>();
			for (LinkedHashMap<String, Object> eachMap : tempList) {
				String currentLobInMap = eachMap.get("name").toString();
				if (currentLobInMap != null && eachLob.equalsIgnoreCase(currentLobInMap)) {
					currentSeriesDataList.add((Long) eachMap.get("value"));
				}
				seriesLobMap.put("data", currentSeriesDataList);
			}
			allSeriesDataList.add(seriesLobMap);
		}

		xAxisComponentsMap.put("type", "category");
		xAxisComponentsMap.put("data", xAxisData);

//		System.err.println("VM:"+xAxisData);

		yAxisComponentsMap.put("type", "value");
		forJson.put("xAxis", xAxisComponentsMap);
		forJson.put("yAxis", yAxisComponentsMap);
		forJson.put("series", allSeriesDataList);

		Gson gson = new Gson();
		String jsonStr = gson.toJson(forJson);

//		System.err.println(jsonStr);
		return jsonStr;
	}

}
