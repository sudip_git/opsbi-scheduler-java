/**
 * Copyright (C) 2018, Exdion Solutions - www.exdion.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package com.sdsl.opsbi.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

/**
 * @author Sudip Das
 *
 */
public class S3BucketHandler {
	/**
	 * Default logger
	 */
	final static Logger logger = Logger.getLogger(S3BucketHandler.class.getName());

	public S3BucketHandler() {
	}

	public AmazonS3 getS3Client() {
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(
				Configurator.awsProps.getProperty("AWSAccessKeyId").toString(),
				Configurator.awsProps.getProperty("AWSSecretKey").toString());
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.AP_SOUTH_1)
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
		return s3Client;
	}

	/**
	 * Uploading log file to S3
	 * 
	 * @param jsonStr    Core Engine generated Job Id
	 * @param currentEnv Current Environment
	 */
	public static void uploadContentToS3(String stringContent, String ouptFileName, String bucketName) {
		logger.info("Uploading File:" + ouptFileName + " to " + bucketName);
		S3BucketHandler s3Writer = new S3BucketHandler();
		s3Writer.getS3Client().putObject(bucketName, ouptFileName, stringContent);

	}

	public static String downloadContentFromS3AsString(String bucketName, String fileName) throws IOException {
		S3BucketHandler s3Reader = new S3BucketHandler();
		S3Object fullObject = s3Reader.getS3Client().getObject(new GetObjectRequest(bucketName, fileName));
		InputStream input = fullObject.getObjectContent();
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		StringBuilder fileContentSB = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			fileContentSB.append(line);
		}
		input.close();
		return fileContentSB.toString();
	}
}
