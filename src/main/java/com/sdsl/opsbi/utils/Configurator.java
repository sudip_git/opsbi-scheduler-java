/**
 * 
 */
package com.sdsl.opsbi.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Instant;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.sdsl.opsbi.exceptions.ConfigException;
import com.sdsl.opsbi.gson.CoverageGsonPojo;
import com.sdsl.opsbi.gson.MetricGsonPojo;
import com.sdsl.opsbi.gson.VolumeGsonPojo;

/**
 * @author Sudip Das
 *
 */
public class Configurator {
	/**
	 * Default logger
	 */
	static Logger logger = null;

	public static Properties awsProps = null;
	public static Properties dbProps = null;
	/**
	 * Location of local log files
	 */
	public static String TEMP_LOG_DIR = "./opsbi_logs/";

	private static String INFRA_JSON = "exdionInfra.json";

	public static String METRIC_CONFIG_JSON = "metric.json";

	public static String CONFIG_BUCKET = "opsbi.ui/config";

	public static String DATA_BUCKET = "opsbi.ui/data";

	public static LinkedHashMap<String, LinkedHashMap<String, ArrayList<String>>> metricConfigMaps = new LinkedHashMap<String, LinkedHashMap<String, ArrayList<String>>>();
	public static HashMap<String, List<String>> orderedKeysParamsMetricMap = new HashMap<String, List<String>>();

	public static void init(String currentEnv) throws ConfigException {
		String logFileName = TEMP_LOG_DIR + currentEnv.toUpperCase() + "_" + getLoggerPrefix() + "_OpsBi.log";
		System.setProperty("logfile.name", logFileName);
		logger = Logger.getLogger(Configurator.class.getName());

		try {
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			String jsonStr = JsonUtil.getJSonToStr(classloader, INFRA_JSON);
			awsProps = getInfraProps(jsonStr, "aws");
			dbProps = getInfraProps(jsonStr, currentEnv.toLowerCase());

			if (awsProps == null) {
				throw new ConfigException("Cloud Configuration Parameters not available, contact Support.");
			}
			if (dbProps == null) {
				throw new ConfigException("Wrong Environment Parameters provided: \"" + currentEnv + "\"");
			}

			/*
			 * Creating the metric map with the sub filters and their values from JSON
			 * 
			 * 
			 * ***********************!! IMPORTANT !!**********************************
			 * WHILE READING FROM FILE FOR GSON OBJECTS ALWAYS CONVERT DATA TO JSON STRING
			 * 
			 * jsonStr = JsonUtil.getJSonToStr(classloader, METRIC_CONFIG_JSON);
			 * 
			 * ***********************!! IMPORTANT !!**********************************
			 * 
			 * 
			 * 
			 */

			jsonStr = S3BucketHandler.downloadContentFromS3AsString(CONFIG_BUCKET, METRIC_CONFIG_JSON);

			Gson gson = new Gson();
			MetricGsonPojo metricJsonData = gson.fromJson(jsonStr, MetricGsonPojo.class);
			VolumeGsonPojo volPojo = metricJsonData.getVolume();
			CoverageGsonPojo covPojo = metricJsonData.getCoverage();

			metricConfigMaps.put("volume", volPojo.getAllParamFilters());
			metricConfigMaps.put("coverage", covPojo.getAllParamFilters());

		} catch (IOException e) {
			logger.error("ERROR: IOException while converting JSON to String or loading Input Stream.");
			throw new ConfigException("In init()", e);
		} catch (ParseException e) {
			logger.error("ERROR: ParseException while converting Nested JSON to HashMap of HashMaps / Bin Maps.");
			throw new ConfigException("In init()", e);
		}
	}

	private static String getLoggerPrefix() {
		Instant instantFromTimestamp = new Instant(System.currentTimeMillis());
		StringBuffer buf = new StringBuffer();
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.year())));
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.monthOfYear())));
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.dayOfMonth())));
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.hourOfDay())));
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.minuteOfDay())));
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.secondOfDay())));
		return buf.toString();
	}

	private static Properties getInfraProps(String jsonStr, String nodeStr) throws ParseException {
		return PropsUtil.getPropsForJsonNode(jsonStr, nodeStr);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
