/**
 * 
 */
package com.sdsl.opsbi.gson;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * @author Sudip Das
 *
 */
public class CoverageGsonPojo {
	String displayName;
	String shortForm;
	String prefix;
	boolean isDisabled;
	boolean isDefault;
	ArrayList<LinkedHashMap<String, String>> of;
	ArrayList<LinkedHashMap<String, String>> against;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getShortForm() {
		return shortForm;
	}

	public void setShortForm(String shortForm) {
		this.shortForm = shortForm;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public boolean isDisabled() {
		return isDisabled;
	}

	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public ArrayList<LinkedHashMap<String, String>> getOf() {
		return of;
	}

	public void setOf(ArrayList<LinkedHashMap<String, String>> of) {
		this.of = of;
	}

	public ArrayList<LinkedHashMap<String, String>> getAgainst() {
		return against;
	}

	public void setAgainst(ArrayList<LinkedHashMap<String, String>> against) {
		this.against = against;
	}

	// Custom method to get list of all filter values for a single param
	public ArrayList<String> getEachParamFilterValues(String param) {
		ArrayList<String> listFromVolPojo = new ArrayList<String>();

		ArrayList<LinkedHashMap<String, String>> paramValuesListFromJson = new ArrayList<LinkedHashMap<String, String>>();
		if (param.equalsIgnoreCase("of"))
			paramValuesListFromJson = getOf();
		else if (param.equalsIgnoreCase("against"))
			paramValuesListFromJson = getAgainst();
		for (LinkedHashMap<String, String> eachOfVolPojo : paramValuesListFromJson) {
			listFromVolPojo.add(eachOfVolPojo.get("key"));
		}
		return listFromVolPojo;
	}

	// Custom method called by configurator to get the filters for the 2 params for
	// coverage metric
	public LinkedHashMap<String, ArrayList<String>> getAllParamFilters() {
		LinkedHashMap<String, ArrayList<String>> allFiltersMap = new LinkedHashMap<String, ArrayList<String>>();
		allFiltersMap.put("of", getEachParamFilterValues("of"));
		allFiltersMap.put("against", getEachParamFilterValues("against"));
		return allFiltersMap;
	}
}
