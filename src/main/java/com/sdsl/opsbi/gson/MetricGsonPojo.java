/**
 * 
 */
package com.sdsl.opsbi.gson;

/**
 * @author Sudip Das
 *
 */
public class MetricGsonPojo {

	VolumeGsonPojo volume;
	CoverageGsonPojo coverage;

	public VolumeGsonPojo getVolume() {
		return volume;
	}

	public void setVolume(VolumeGsonPojo volume) {
		this.volume = volume;
	}

	public CoverageGsonPojo getCoverage() {
		return coverage;
	}

	public void setCoverage(CoverageGsonPojo coverage) {
		this.coverage = coverage;
	}

}
