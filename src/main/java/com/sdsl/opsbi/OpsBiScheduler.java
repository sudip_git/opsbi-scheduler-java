/**
 * 
 */
package com.sdsl.opsbi;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.javatuples.Pair;
import org.joda.time.LocalDate;
import org.json.simple.parser.ParseException;

import com.sdsl.opsbi.db.DBController;
import com.sdsl.opsbi.exceptions.ConfigException;
import com.sdsl.opsbi.exceptions.DBException;
import com.sdsl.opsbi.metric.CoverageMetricQs;
import com.sdsl.opsbi.metric.IMetric;
import com.sdsl.opsbi.metric.VolumeMetric;
import com.sdsl.opsbi.utils.Configurator;
import com.sdsl.opsbi.utils.S3BucketHandler;

/**
 * @author Sudip Das
 *
 */
//public class OpsBiScheduler extends TimerTask {
public class OpsBiScheduler {
	/**
	 * Default logger
	 */
	static Logger logger = null;

	private ClassLoader classloader = Thread.currentThread().getContextClassLoader();

	/**
	 * Database controller
	 */
	private DBController controller = null;

	private static String COV_QS_PROP_FILE = "coverage_qs.properties";
	public static Properties covQsProps = new Properties();

	private static ArrayList<String> topLobs = null;

	/**
	 * Chart Type
	 */
	public final static int LINE_CHART = 1;
	public final static int HEAT_MAP = 2;
	public final static int BAR_CHART = 3;
	public final static int PIE_CHART = 4;
	public final static int TREEMAP = 5;
	public final static int NUMBER_CARD = 6;
	public final static int GAUGE = 7;
	public final static int BUBBLE_CHART = 8;
	public final static int DEPENDENCY_ECHART = 9;
	public final static int SUNBURST_ECHART = 10;

	public final static String[] chartNames = { "bar", "pie", "line", "bubble", "heatmap", "treemap", "numbercard",
			"gauge" };

//	public final static String[] chartNames = { "bubble","heatmap", "line" };

	/**
	 * 
	 * @param currentEnv
	 * @throws ConfigException
	 * @throws DBException
	 * @throws IOException
	 */

	public OpsBiScheduler(String currentEnv) throws ConfigException, DBException, IOException {

		Configurator.init(currentEnv);
		logger = Logger.getLogger(OpsBiScheduler.class.getName());

		InputStream is = classloader.getResourceAsStream(COV_QS_PROP_FILE);
		covQsProps.load(is);
		Set<Object> covQsPropsKeys = covQsProps.keySet();

		for (Object eachCovQs : covQsPropsKeys) {
			CoverageMetricQs.coverageQsList.add(eachCovQs.toString());
		}

		controller = new DBController();
	}

	/**
	 * Get the top ten scored LOBs
	 * 
	 * @return
	 */
	public static ArrayList<String> getTopLobs() {
		return topLobs;
	}

	/**
	 * The Bi scheduler method
	 * 
	 * @throws DBException
	 * @throws ParseException
	 */
	public void runScheduler() throws DBException, ParseException {

		Date currentDate = new Date();
//		currentDate = Date.from(Instant.parse("2020-05-01T00:00:00.000Z"));

		// IMPORTANT STEP BEFORE GENERATING JSONS, ALSO ADD OTHERS AS LOB
		topLobs = controller.getTopTenLobs();
		topLobs.add("OTHERS");

		LinkedHashMap<String, LinkedHashMap<String, ArrayList<String>>> allOpsBiDataAsMap = Configurator.metricConfigMaps;

		LinkedHashMap<String, ArrayList<String>> currentMetricDataMap = allOpsBiDataAsMap.get("volume");

		generateVolumeMetricJsons(controller, currentDate, currentMetricDataMap);

		currentMetricDataMap = allOpsBiDataAsMap.get("coverage");

		generateCoverageMetricJsons(controller, currentDate, currentMetricDataMap, topLobs);

		// Echarts
		generateCoverageEchartDependencyJsons(controller, currentDate, currentMetricDataMap, topLobs);

		generateCoverageEchartSunburst(controller, currentDate, currentMetricDataMap, topLobs);

		controller.controller_finalize();

	}

	/**
	 * Start and End Dates associated with the flattened table data fetch
	 * 
	 * @param date
	 * @return
	 */
	public Pair<Timestamp, Timestamp> getBigTableFetchStartEndTs(Date date) {
		LocalDate finalLocalDateEnd = LocalDate.fromDateFields(date);
		finalLocalDateEnd = finalLocalDateEnd.minusDays(1);// Minus the current date
		Date finalDateEnd = finalLocalDateEnd.toDate();

		LocalDate finalLocalDateStart = finalLocalDateEnd.minusYears(1);
		Date finalDateStart = finalLocalDateStart.toDate();
		Pair<Timestamp, Timestamp> startEndTsPair = Pair.with(new Timestamp(finalDateStart.getTime()),
				new Timestamp(finalDateEnd.getTime()));
		return startEndTsPair;
	}

	/**
	 * Generating json for Volume of Files, Packs, Extracts,Pages in Top Ten LOBs
	 * 
	 * @param currentDate
	 * 
	 * @throws DBException
	 * @throws ParseException
	 */
	public void generateVolumeMetricJsons(DBController controller, Date currentDate,
			LinkedHashMap<String, ArrayList<String>> currentMetricDataMap) throws DBException, ParseException {

		ArrayList<String> metricOfValsList = currentMetricDataMap.get("of");
		ArrayList<String> metricAgainstValsList = currentMetricDataMap.get("against");
		ArrayList<String> metricStackByValsList = currentMetricDataMap.get("stackBy");

		/*
		 * NEED TO REMOVE BELOW LINES
		 */
		System.err
				.println("OpsBiScheduler.generateVolumeMetricJsons: \"Pages\" not configured in DB: removing metric.");
		metricOfValsList.remove("pages");
		/*
		 * ABOVE LINES TO BE REMOVED
		 */

		for (String eachChartName : chartNames) {
			int chartType = getChartCodeForChartName(eachChartName);
			if (chartType == -1) {
				System.err.println("CHART NOT RECONGNIZED: " + eachChartName);
				logger.error("CHART NOT RECONGNIZED: " + eachChartName);
				continue;
			}

//			boolean generateExtraNoStackByFiles = isVolumeNoStackByFilesRequired(chartType);

			for (String eachMetricOfVal : metricOfValsList) {
				int metricOfId = VolumeMetric.getMetricOfIdForMetricName(eachMetricOfVal);
				if (metricOfId == -1) {
					System.err.println("METRIC ID NOT RECONGNIZED: " + eachMetricOfVal);
					logger.error("METRIC ID NOT RECONGNIZED: " + eachMetricOfVal);
					continue;
				}

				VolumeMetric metricObj = new VolumeMetric();

				for (String eachMetricAgainstVal : metricAgainstValsList) {
					int againstId = VolumeMetric.getAgainstCodeForAgainstStr(eachMetricAgainstVal);

//					System.out.println("Period:"+eachMetricAgainstVal);

					if (againstId == -1) {
						System.err.println("AGAINST ID NOT RECONGNIZED: " + eachMetricAgainstVal);
						logger.error("AGAINST ID NOT RECONGNIZED: " + eachMetricAgainstVal);
						continue;
					}
					for (String eachMetricStackByVal : metricStackByValsList) {
						int stackById = VolumeMetric.getStackByCodeForStackByStr(eachMetricStackByVal);
						if (stackById == -1) {
							System.err.println("STACK BY ID NOT RECONGNIZED: " + eachMetricStackByVal);
							logger.error("STACK BY ID NOT RECONGNIZED: " + eachMetricStackByVal);
							continue;
						}
						String fileStr = "";
						String jsonStr = "";
						if (isFileForStackByApplicable(chartType)) {
							fileStr = eachChartName + "_volume_" + eachMetricOfVal + "_" + eachMetricAgainstVal + "_"
									+ eachMetricStackByVal + ".json";
							jsonStr = VolumeMetric.getJsonString(controller, currentDate, metricOfId, againstId,
									stackById, chartType);
							jsonStr = postProcessJsonStr(metricObj, jsonStr, chartType, true);
//							System.out.println(fileStr + ":" + jsonStr);
							S3BucketHandler.uploadContentToS3(jsonStr, fileStr, Configurator.DATA_BUCKET);
						}
						/*
						 * Files irrespective of stack by for all charts
						 */
						fileStr = eachChartName + "_volume_" + eachMetricOfVal + "_" + eachMetricAgainstVal + ".json";
						jsonStr = VolumeMetric.getJsonString(controller, currentDate, metricOfId, againstId,
								IMetric.NO_GROUPBY, chartType);
						jsonStr = postProcessJsonStr(metricObj, jsonStr, chartType, false);
//						System.err.println(fileStr+":"+jsonStr);
						S3BucketHandler.uploadContentToS3(jsonStr, fileStr, Configurator.DATA_BUCKET);
					}
				}
			}
		}
	}

	/**
	 * Get chart code corresponding to chart name
	 * 
	 * @param chartName
	 * @return
	 */
	public int getChartCodeForChartName(String chartName) {
		chartName = chartName.trim();
		if (chartName.equalsIgnoreCase("line")) {
			return LINE_CHART;
		} else if (chartName.equalsIgnoreCase("heatmap")) {
			return HEAT_MAP;
		} else if (chartName.equalsIgnoreCase("bar")) {
			return BAR_CHART;
		} else if (chartName.equalsIgnoreCase("pie")) {
			return PIE_CHART;
		} else if (chartName.equalsIgnoreCase("treemap")) {
			return TREEMAP;
		} else if (chartName.equalsIgnoreCase("numbercard")) {
			return NUMBER_CARD;
		} else if (chartName.equalsIgnoreCase("gauge")) {
			return GAUGE;
		} else if (chartName.equalsIgnoreCase("bubble")) {
			return BUBBLE_CHART;
		} else {
			return -1;
		}
	}

	/**
	 * Is stackBy files applicable or only against
	 * 
	 * @param chartType
	 * @return
	 */
	public boolean isFileForStackByApplicable(int chartType) {
		boolean isAppliacble = false;

		switch (chartType) {
		case LINE_CHART:
			isAppliacble = true;
			break;
		case HEAT_MAP:
			isAppliacble = true;
			break;
		case BAR_CHART:
			isAppliacble = true;
			break;
		case PIE_CHART:
			isAppliacble = false;
			break;
		case TREEMAP:
			isAppliacble = false;
			break;
		case NUMBER_CARD:
			isAppliacble = false;
			break;
		case GAUGE:
			isAppliacble = false;
			break;
		case BUBBLE_CHART:
			isAppliacble = true;
			break;
		}
		return isAppliacble;
	}

	/**
	 * Post Processing of line, heatmap, bubble
	 * 
	 * @param metricObj
	 * @param jsonStrToProcess
	 * @param chartType
	 * @param isGroupBy
	 * @return
	 * @throws ParseException
	 */
	public String postProcessJsonStr(VolumeMetric metricObj, String jsonStrToProcess, int chartType, boolean isGroupBy)
			throws ParseException {
		String returnJsonStr = jsonStrToProcess;

		switch (chartType) {
		case LINE_CHART:
			if (isGroupBy)
				returnJsonStr = metricObj.postprocessEchartLineGroupJsonString(jsonStrToProcess);
			else
				returnJsonStr = metricObj.postprocessEchartLineNoGroupJsonString(jsonStrToProcess);
			break;
		case HEAT_MAP:
			if (isGroupBy)
				returnJsonStr = metricObj.postprocessHeatGroupJsonString(jsonStrToProcess);
			else
				returnJsonStr = metricObj.postprocessHeatNoGroupJsonString(jsonStrToProcess);
			break;
		case BAR_CHART:
			if (isGroupBy)
				returnJsonStr = metricObj.postprocessEchartBarGroupJsonString(jsonStrToProcess);
			else
				returnJsonStr = metricObj.postprocessEchartBarNoGroupJsonString(jsonStrToProcess);
			break;
		case PIE_CHART:
			returnJsonStr = jsonStrToProcess;
			break;
		case TREEMAP:
			returnJsonStr = jsonStrToProcess;
			break;
		case NUMBER_CARD:
			returnJsonStr = jsonStrToProcess;
			break;
		case GAUGE:
			returnJsonStr = jsonStrToProcess;
			break;
		case BUBBLE_CHART:
			if (isGroupBy)
				returnJsonStr = metricObj.postprocessBubbleGroupJsonString(jsonStrToProcess);
			else
				returnJsonStr = metricObj.postprocessBubbleNoGroupJsonString(jsonStrToProcess);
			break;
		}

//		if(chartType==BAR_CHART) {
//			System.err.println("returnJsonStr:"+returnJsonStr);
//		}
		return returnJsonStr;
	}

	public void generateCoverageMetricJsons(DBController controller, Date currentDate,
			LinkedHashMap<String, ArrayList<String>> currentMetricDataMap, ArrayList<String> lobList)
			throws DBException {

		CoverageMetricQs coverageMetric = new CoverageMetricQs();

		ArrayList<String> metricAgainstValsList = currentMetricDataMap.get("against");

		for (String eachChartName : chartNames) {
			int chartType = getChartCodeForChartName(eachChartName);
			if (chartType == -1) {
				System.err.println("CHART NOT RECONGNIZED: " + eachChartName);
				logger.error("CHART NOT RECONGNIZED: " + eachChartName);
				continue;
			}

			for (String eachMetricAgainstVal : metricAgainstValsList) {
				int againstId = CoverageMetricQs.getAgainstCodeForAgainstStr(eachMetricAgainstVal);
				if (againstId == -1) {
					System.err.println("AGAINST ID NOT RECONGNIZED: " + eachMetricAgainstVal);
					logger.error("AGAINST BY ID NOT RECONGNIZED: " + eachMetricAgainstVal);
					continue;
				}
				switch (chartType) {
				case LINE_CHART:
					coverageMetric.generateEchartBarLineCoverageJsons(controller, chartType, againstId);
					break;
				case HEAT_MAP:
					coverageMetric.generateLineHeatmapCoverageJsons(controller, chartType, againstId);
					break;
				case BAR_CHART:
					coverageMetric.generateEchartBarLineCoverageJsons(controller, chartType, againstId);
					break;
				case PIE_CHART:
					coverageMetric.generatePieTreeCardGaugeCoverageJsons(controller, chartType, againstId);
					break;
				case TREEMAP:
					coverageMetric.generatePieTreeCardGaugeCoverageJsons(controller, chartType, againstId);
					break;
				case NUMBER_CARD:
					coverageMetric.generatePieTreeCardGaugeCoverageJsons(controller, chartType, againstId);
					break;
				case GAUGE:
					coverageMetric.generatePieTreeCardGaugeCoverageJsons(controller, chartType, againstId);
					break;
				case BUBBLE_CHART:
					coverageMetric.generateBubbleCoverageJsons(controller, chartType, againstId);
					break;
				}
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Environment not metioned. Aborting...");
			System.exit(1);
		}
		String currentEnv = args[0].trim().toLowerCase();

		System.out.println("*******************************************************************************");
		System.out.println("		Sumyag BI Scheduler Version 1.0.0430");
		System.out.println("*******************************************************************************");
		System.out.println();
		System.out.println("            #########################################");
		System.out.println("			Build Tag 20200430");
		System.out.println("            #########################################");
		System.out.println();

		try {
			OpsBiScheduler scheduler = new OpsBiScheduler(currentEnv);
			scheduler.runScheduler();

			logger.info("Schedule run completed......");
			System.exit(0);

		} catch (ConfigException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (DBException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (ParseException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * @param controller
	 * @param currentDate
	 * @param currentMetricDataMap
	 * @param topLobs
	 * @throws DBException
	 */
	private void generateCoverageEchartDependencyJsons(DBController controller, Date currentDate,
			LinkedHashMap<String, ArrayList<String>> currentMetricDataMap, ArrayList<String> topLobs)
			throws DBException {

		CoverageMetricQs coverageMetric = new CoverageMetricQs();
		coverageMetric.generateEchartDependencyGraph(controller, DEPENDENCY_ECHART, IMetric.LOB);
	}

	/**
	 * @param controller
	 * @param currentDate
	 * @param currentMetricDataMap
	 * @param topLobs
	 * @throws DBException
	 */
	private void generateCoverageEchartSunburst(DBController controller, Date currentDate,
			LinkedHashMap<String, ArrayList<String>> currentMetricDataMap, ArrayList<String> topLobs)
			throws DBException {

		CoverageMetricQs coverageMetric = new CoverageMetricQs();
		coverageMetric.generateEchartSunburst(controller, SUNBURST_ECHART, IMetric.LOB);
	}
}
