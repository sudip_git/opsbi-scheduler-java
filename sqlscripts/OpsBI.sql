--Table creation
-- VOLUMES: FILES, Packs, Extracts, Pages

CREATE TABLE public.opsbicarrierlob (
	list_time TIMESTAMP,
	pack_id TEXT,
	lob TEXT ,
	qs_key TEXT,
	totalfiles INTEGER,
	totalpacks INTEGER,
	totalextracts INTEGER,
	totalpages INTEGER
); 

CREATE TABLE public.opsbiclientregion (
	list_time TIMESTAMP,
	client TEXT,
	region TEXT ,
	totalfiles INTEGER,
	totalpacks INTEGER,
	totalextracts INTEGER,
	totalpages INTEGER
); 

CREATE TABLE public.opsbicarrierdict (
	pack_id TEXT,
	carriervalue TEXT ,
	carriervaluestd TEXT
); 

CREATE TABLE public.toplobs (
	file_lob TEXT
); 

--Table creation: Coverage-Questions

CREATE TABLE public.opsbicoverageqslob (
	qs_key TEXT,
	lob TEXT ,
	extrinlobcount INTEGER,
	qsextrtcount INTEGER
);



DROP TABLE opsbicarrierlob
DROP TABLE opsbiclientregion
DROP TABLE opsbicoverageqslob
--********************************** STORED PROCEDURES VOLUME **********************************************

-- 1. Cube: Carrier and LOB file count, pack count, extraction count
--dataCarrierLobDataPS
    
SELECT n1.list_time, n1.pack_id,n1.file_lob, n1.qs_key, 
    n2.totalfiles, n2.totalpacks, n2.totalextracts,n2.totalpages
    FROM (SELECT 
            epod_flattened.job_id AS job_id,
            epod_flattened.pack_id AS pack_id,
            epod_flattened.job_status_ts:: TIMESTAMP AS "list_time",
            epod_flattened.file_lob AS file_lob,
            epod_flattened.qs_key AS qs_key
            FROM epod_flattened
            WHERE  
            epod_flattened.job_status_ts BETWEEN
            '2019-09-01T00:00:00.000Z' :: TIMESTAMP  AND '2019-11-01T00:00:00.000Z' :: TIMESTAMP 
            GROUP BY 
            CUBE (job_id,pack_id,list_time,file_lob,qs_key)
            ORDER BY 
            list_time) n1
INNER JOIN (SELECT 
                epod_flattened.job_id AS job_id,
                count(DISTINCT epod_flattened.file_id) AS totalfiles ,
                count(DISTINCT epod_flattened.pack_id) AS totalpacks,
                count(DISTINCT epod_flattened.extr_id) AS totalextracts,
                count(DISTINCT epod_flattened.page_num) AS totalpages
                FROM epod_flattened 
                GROUP BY job_id) n2
ON n1.job_id=n2.job_id



-- 2. Cube: Client and Region pack count 
--dataClientRegionDataPS

SELECT 
--epod_flattened.job_status_ts :: DATE AS "list_time",
epod_flattened.job_status_ts :: TIMESTAMP AS "list_time",
epod_flattened.client AS client,
epod_flattened.geo AS region,
count(DISTINCT epod_flattened.file_id) AS totalfiles,
count(DISTINCT epod_flattened.pack_id) AS totalpacks,
count(DISTINCT epod_flattened.extr_id) AS totalextracts,
count(DISTINCT epod_flattened.page_num) AS totalpages
FROM epod_flattened
WHERE  
    epod_flattened.job_status_ts BETWEEN
    '2019-09-09T00:00:00.000Z' :: TIMESTAMP  AND '2019-10-10T00:00:00.000Z' :: TIMESTAMP 
GROUP BY 
    CUBE (list_time,client,region)
ORDER BY 
    list_time

-- 3. Cube: Carrier Enrichment for Dictionary Packid and Carrier resolution with enrichment
--dataCarrierEnrichmentForCarrierDictPS
SELECT DISTINCT 
    epod_flattened.pack_id, 
    epod_flattened.qs_value AS carriervalue, 
    epod_flattened.qs_value_std AS carriervaluestd
FROM epod_flattened 
WHERE 
    epod_flattened.job_status_ts BETWEEN
    '2019-09-01T00:00:00.000Z' :: TIMESTAMP  AND '2019-11-01T00:00:00.000Z' :: TIMESTAMP 
    AND
    epod_flattened.qs_key ='UC1'

--Last 30 days json
SELECT list_time::DATE AS list_time, totalfiles
FROM opsbicarrierlob WHERE list_time 
BETWEEN  '2019-12-12 00:00:00.0':: TIMESTAMP  
AND  '2019-12-18 00:00:00.0':: TIMESTAMP
GROUP BY 
    list_time,totalfiles
ORDER BY list_time

--Last 30 days json grouped by LOB
SELECT lob, totalfiles
FROM opsbicarrierlob WHERE list_time 
BETWEEN  '2019-12-13 00:00:00.0':: TIMESTAMP  
AND  '2019-12-19 00:00:00.0':: TIMESTAMP
AND lob<> 'NO RECORDS'
GROUP BY 
    lob, totalfiles
ORDER BY lob


-- scoring the LOBs top ten
--dataTopTenLobsPS

SELECT upper(epod_flattened.file_lob) AS file_lob, count( DISTINCT epod_flattened.file_id) AS "file_volume" 
FROM epod_flattened 
WHERE epod_flattened.file_lob IS NOT NULL AND 
epod_flattened.file_id IS NOT NULL
GROUP BY upper(epod_flattened.file_lob) ORDER BY file_volume DESC
LIMIT 10




DELETE FROM opsbicarrierlob WHERE EXISTS (SELECT *  FROM opsbicarrierlob )
DELETE FROM opsbiclientregion WHERE EXISTS (SELECT *  FROM opsbiclientregion )
DELETE FROM opsbicarrierdict WHERE EXISTS (SELECT *  FROM opsbicarrierdict )

SELECT count( * ) FROM opsbicarrierlob
SELECT count( * ) FROM opsbiclientregion
SELECT count( * ) FROM opsbicarrierdict

SELECT * FROM toplobs
SELECT * FROM opsbicarrierlob LIMIT 5
SELECT * FROM opsbiclientregion LIMIT 5
--*********************************************** END USAGE *******************************************

--********************************** STORED PROCEDURES COVERAGE ***************************************

SELECT subT1.qs_key AS qs_key,subT1.file_lob AS lob, subT1.extrinlobcount AS extrinlobcount, subT2.qsextrtcount AS qsextrtcount   
FROM ( 
	SELECT qs_key, file_lob,count(*) AS extrinlobcount FROM public.epod_flattened WHERE qs_key IS NOT NULL AND file_lob IS NOT NULL 
	 GROUP BY CUBE(qs_key,file_lob)
	 ORDER BY qs_key
	 ) AS subT1 
INNER JOIN(
	SELECT qs_key,count(*) AS qsextrtcount FROM public.epod_flattened WHERE qs_key IS NOT NULL AND file_lob IS NOT NULL
	GROUP BY qs_key) AS subT2
ON subT1.qs_key=subT2.qs_key



--*********************************************** END USAGE *******************************************





















--*********************************************** TRIGGERS SECTION *******************************************

-- Trigger to update opsbicarrierlob when epod_flattened gets updated

CREATE OR REPLACE FUNCTION insert_opsbicarrierlob()
  RETURNS TRIGGER AS
$$
BEGIN
         INSERT INTO opsbicarrierlob(list_time,pack_id,lob,qs_key,totalfiles,totalpacks,totalextracts,     totalpages)
         VALUES(NEW.list_time,NEW.pack_id,NEW.lob,NEW.qs_key,NEW.totalfiles,NEW.totalpacks,NEW.totalextracts,     NEW.totalpages);
 
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER ins_same_rec
    AFTER INSERT
    ON epod_flattened
    FOR EACH ROW
    EXECUTE PROCEDURE insert_opsbicarrierlob()
    
--******************************************** END TRIGGERS SECTION ******************************************






