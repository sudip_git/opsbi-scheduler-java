import pandas as pd
from epod2.db_handler.postgres_operations import PostgresHandler


db = PostgresHandler()


def get_last_sync_date(table_name="opsbicarrierlob", ts_col_name='list_time'):
    sql_query = f"SELECT max({ts_col_name}) as last_sync_date FROM {table_name}"
    
    last_sync_date = db.select_query(sql_query=sql_query, default_return=None, fetch_col_name='last_sync_date', fetch_multiple=False)
    
    return last_sync_date


def get_carrier_lob_data(start_date):
    df = None
    try:
        sql_query = f"""
            SELECT n1.list_time, n1.pack_id, n1.file_lob AS lob, n1.qs_key,
                n2.totalfiles, n2.totalpacks, n2.totalextracts, n2.totalpages
            FROM (SELECT
                    epod_flattened.job_id AS job_id,
                    epod_flattened.pack_id AS pack_id,
                    epod_flattened.job_status_ts:: TIMESTAMP AS "list_time",
                    epod_flattened.file_lob AS file_lob,
                    epod_flattened.qs_key AS qs_key
                FROM epod_flattened
                WHERE  
                        epod_flattened.job_status_ts > '{start_date}'::TIMESTAMP
                        GROUP BY 
                        CUBE (job_id,pack_id,list_time,file_lob,qs_key)
                        ORDER BY  list_time) n1
            INNER JOIN
            (SELECT
                epod_flattened.job_id AS job_id,
                count(DISTINCT epod_flattened.file_id) AS totalfiles ,
                count(DISTINCT epod_flattened.pack_id) AS totalpacks,
                count(DISTINCT epod_flattened.extr_id) AS totalextracts,
                count(DISTINCT epod_flattened.page_num) AS totalpages
            FROM epod_flattened
            GROUP BY job_id)
            n2
            ON n1.job_id=n2.job_id
        """

        df = db.pg.select_rows(sql_query)

    except Exception as ex:
        print("Error encountered while being carrier lob data")

    finally:
        return df


def get_client_region_data(start_date):
    df = None
    try:
        sql_query = f"""
            SELECT
                epod_flattened.job_status_ts::TIMESTAMP AS "list_time",
                epod_flattened.client AS client,
                epod_flattened.geo AS region,
                count(DISTINCT epod_flattened.file_id) AS totalfiles,
                count(DISTINCT epod_flattened.pack_id) AS totalpacks,
                count(DISTINCT epod_flattened.extr_id) AS totalextracts,
                count(DISTINCT epod_flattened.page_num) AS totalpages
            FROM epod_flattened
            WHERE  
                epod_flattened.job_status_ts > '{start_date}'::TIMESTAMP
            GROUP BY 
                CUBE (list_time,client,region)
            ORDER BY 
                list_time
        """

        df = db.pg.select_rows(sql_query)

    except Exception as ex:
        print("Error encountered while being client region data")

    finally:
        return df


def get_carrier_data(start_date, carrier_key="UC1"):
    df = None
    try:
        sql_query = f"""
            SELECT DISTINCT
                epod_flattened.pack_id,
                epod_flattened.qs_value AS carriervalue,
                epod_flattened.qs_value_std AS carriervaluestd
            FROM epod_flattened
            WHERE 
                epod_flattened.job_status_ts > '{start_date}'::TIMESTAMP
                AND
                epod_flattened.qs_key ='{carrier_key}'
        """

        df = db.pg.select_rows(sql_query)

    except Exception as ex:
        print("Error encountered while being carrier data")

    finally:
        return df


def get_coverage_data(start_date):
    df = None
    try:
        sql_query = f"""
            SELECT subT1.qs_key AS qs_key, subT1.file_lob AS lob, subT1.extrinlobcount AS extrinlobcount, subT2.qsextrtcount AS qsextrtcount
            FROM ( 
                SELECT qs_key, file_lob, count(*) AS extrinlobcount
                FROM public.epod_flattened
                WHERE qs_key IS NOT NULL AND file_lob IS NOT NULL
                AND epod_flattened.job_status_ts > '{start_date}'::TIMESTAMP
                GROUP BY CUBE(qs_key,file_lob)
                ORDER BY qs_key
            ) AS subT1
            INNER JOIN(
            SELECT qs_key, count(*) AS qsextrtcount
            FROM public.epod_flattened
            WHERE qs_key IS NOT NULL AND file_lob IS NOT NULL
            AND epod_flattened.job_status_ts > '{start_date}'::TIMESTAMP
            GROUP BY qs_key) AS subT2
            ON subT1.qs_key=subT2.qs_key
        """

        df = db.pg.select_rows(sql_query)

    except Exception as ex:
        print("Error encountered while being coverage data")

    finally:
        return df


# print("Fetching last sync update for opsbicarrierlob")
# start_date = get_last_sync_date()
# print(f"Last sync data is {start_date}")

# print("Fetching carried lob data")
# carrier_lob_df = get_carrier_lob_data(start_date)

# print("Inserting carrier lob data")
# print("Handling None values")
# carrier_lob_df = carrier_lob_df.where(pd.notnull(carrier_lob_df), None).copy()
# carrier_lob_df[carrier_lob_df.select_dtypes('datetime').columns] = carrier_lob_df.select_dtypes('datetime').replace({pd.NaT: None})
# db.pg.insert_batch(df=carrier_lob_df, table="opsbicarrierlob")

start_date = '2020-02-03 12:57:11.332300'

print("Fetching client region data")
client_region_df = get_client_region_data(start_date)

print("Inserting Client Region Data")
client_region_df = client_region_df.where(pd.notnull(client_region_df), None).copy()
client_region_df[client_region_df.select_dtypes('datetime').columns] = client_region_df.select_dtypes('datetime').replace({pd.NaT: None})
db.pg.insert_batch(df=client_region_df, table="opsbiclientregion")

print("Fetching carrier data")
carrier_df = get_carrier_data(start_date)

print("Inserting carrier data")
carrier_df = carrier_df.where(pd.notnull(carrier_df), None).copy()
carrier_df[carrier_df.select_dtypes('datetime').columns] = carrier_df.select_dtypes('datetime').replace({pd.NaT: None})
db.pg.insert_batch(df=carrier_df, table="opsbicarrierdict")

print("Fetching Coverage data")
coverage_df = get_coverage_data(start_date)

print("Inserting coverage data")
coverage_df = coverage_df.where(pd.notnull(coverage_df), None).copy()
coverage_df[coverage_df.select_dtypes('datetime').columns] = coverage_df.select_dtypes('datetime').replace({pd.NaT: None})
db.pg.insert_batch(df=coverage_df, table="opsbicoverageqslob")
