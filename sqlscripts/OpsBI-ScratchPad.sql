SELECT count( * ) FROM opsbicarrierlob
SELECT count( * ) FROM opsbiclientregion
SELECT count( * ) FROM opsbicarrierdict

DELETE FROM opsbicarrierlob WHERE EXISTS (SELECT *  FROM opsbicarrierlob )
DELETE FROM opsbiclientregion WHERE EXISTS (SELECT *  FROM opsbiclientregion )
DELETE FROM opsbicarrierdict WHERE EXISTS (SELECT *  FROM opsbicarrierdict )

SELECT * FROM epod_flattend LIMIT 10

SELECT * FROM opsbicarrierlob LIMIT 50

SELECT list_time::DATE AS list_time, totalfiles
FROM opsbicarrierlob WHERE list_time 
BETWEEN  '2019-12-12 00:00:00.0':: TIMESTAMP  
AND  '2019-12-18 00:00:00.0':: TIMESTAMP
GROUP BY 
    list_time,totalfiles
ORDER BY list_time

select * from opsbicoverageqslob limit 5000
where qs_key='FN1' 
qsextrtcount=8099


SELECT n1.list_time, n1.pack_id,n1.file_lob, n1.qs_key, 
    n2.totalfiles, n2.totalpacks, n2.totalextracts,n2.totalpages
    FROM (SELECT 
            epod_flattened.job_id AS job_id,
            epod_flattened.pack_id AS pack_id,
            epod_flattened.job_status_ts:: TIMESTAMP AS "list_time",
            epod_flattened.file_lob AS file_lob,
            epod_flattened.qs_key AS qs_key
            FROM epod_flattened
            WHERE  
            epod_flattened.job_status_ts BETWEEN
            '2020-03-26T00:00:00.000Z' :: TIMESTAMP  AND '2020-03-27T00:00:00.000Z' :: TIMESTAMP 
            GROUP BY 
            job_id,pack_id,list_time,file_lob,qs_key
            --CUBE (job_id,pack_id,list_time,file_lob,qs_key)
            ORDER BY 
            list_time) n1
INNER JOIN (SELECT 
                epod_flattened.job_id AS job_id,
                count(DISTINCT epod_flattened.file_id) AS totalfiles ,
                count(DISTINCT epod_flattened.pack_id) AS totalpacks,
                count(DISTINCT epod_flattened.extr_id) AS totalextracts,
                count(DISTINCT epod_flattened.page_num) AS totalpages
                FROM epod_flattened 
                GROUP BY job_id) n2
ON n1.job_id=n2.job_id

select month, day, count (distinct file_id) from epod_flattened 
where epod_flattened.job_status_ts BETWEEN 
'2020-03-01T00:00:00.000Z' :: TIMESTAMP  AND '2020-04-03T00:00:00.000Z' :: TIMESTAMP 
group by month, day
order by month, day desc

Select count(distinct file_id), file_lob from epod_flattened 
where epod_flattened.job_status_ts BETWEEN 
'2020-03-19T00:00:00.000Z' :: TIMESTAMP  AND '2020-03-20T00:00:00.000Z' :: TIMESTAMP 
and file_lob is not null 
group by file_lob

--Daily query: 325 count
SELECT COUNT(distinct file_id),
EXTRACT(DAY FROM file_status_ts) as day,
EXTRACT(MONTH FROM file_status_ts) as month,
EXTRACT(YEAR FROM file_status_ts) as year
FROM files WHERE job_id in (
    SELECT job_id FROM jobs
    WHERE job_status_ts >= '2020-03-26'::timestamp
    AND job_status_ts <= '2020-03-27'::timestamp
AND job_owner = 'policycheck@exdionpod.com' 
)
GROUP BY year, month, day
ORDER BY year, month

--Verfication main: 
select count (distinct file_id) 
from epod_flattened where job_status_ts BETWEEN 
'2020-03-26T00:00:00.000Z' :: TIMESTAMP  AND '2020-03-27T00:00:00.000Z' :: TIMESTAMP and job_owner = 'policycheck@exdionpod.com' 

--Verification1:321 count
select count (distinct file_id) from epod_flattened  
WHERE 
job_status_ts >= '2020-03-26'::timestamp AND job_status_ts <= '2020-03-27'::timestamp 
and job_owner = 'policycheck@exdionpod.com' 

--Getting total files count: 271
SELECT distinct(list_time), totalfiles  FROM opsbicarrierlob WHERE list_time  
BETWEEN  '2020-03-26' :: TIMESTAMP AND  '2020-03-27' :: TIMESTAMP --AND lob<> 'NO RECORDS' 

--Verification2
select count (distinct file_id) ,
EXTRACT(DAY FROM job_status_ts) as day,
EXTRACT(MONTH FROM job_status_ts) as month,
EXTRACT(YEAR FROM job_status_ts) as year 
from epod_flattened 
	where job_status_ts BETWEEN  '2020-03-26T00:00:00.000Z' :: TIMESTAMP  AND '2020-03-27T00:00:00.000Z' :: TIMESTAMP 
	and job_owner = 'policycheck@exdionpod.com' 
GROUP BY year, month, day, job_status_ts
ORDER BY year, month



select count (distinct file_id) 
from epod_flattened where job_status_ts BETWEEN 
'2020-03-26T00:00:00.000Z' :: TIMESTAMP  AND '2020-03-27T00:00:00.000Z' :: TIMESTAMP and job_owner = 'policycheck@exdionpod.com' 




select distinct(file_id), file_lob,pack_id, qs_key from epod_flattened 
where job_status_ts BETWEEN 
'2020-03-30T00:00:00.000Z' :: TIMESTAMP  AND '2020-03-31T00:00:00.000Z' :: TIMESTAMP
and file_lob='UMB'

SELECT lob, totalfiles  FROM opsbicarrierlob WHERE list_time  
BETWEEN  '2020-03-30' :: TIMESTAMP AND  '2020-03-31' :: TIMESTAMP --AND lob<> 'NO RECORDS' 
GROUP BY lob, totalfiles order by lob


select * from opsbi_volume_coverage where extracted_on between '2020-03-01'::date and '2020-03-31' :: date


select * from opsbi_volume_coverage where extracted_on between '2020-04-14'::date and '2020-04-20' :: date and file_lob is not null and qs_key is null 

select * from opsbi_volume_coverage where file_lob ='AUT' and qs_key ='AD1' 

select distinct (qs_key) from opsbi_volume_coverage where qs_key is not null

select file_lob, sum(total_extracts) as sum_extracts from opsbi_volume_coverage where file_lob is not null and qs_key is null group by file_lob

select file_lob, qs_key, sum(total_extracts) as sum_qs_extracts from opsbi_volume_coverage where file_lob is not null and qs_key is NOT null group by file_lob, qs_key

